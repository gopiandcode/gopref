[@@@warning "-32"]
open Containers
open Brr
open Utils

let ($) (a,b,c,d) = prop a b c d

let build_load_fn f =
  fun v -> f ~total:(Jv.to_int (Jv.get v "total")) ~loaded:(Jv.to_int (Jv.get v "loaded"))

let bool_field to_jv name = prop name to_jv Jv.of_bool Jv.to_bool
let string_field to_jv name = prop name to_jv Jv.of_string Jv.to_string
let int_field to_jv name = prop name to_jv Jv.of_int Jv.to_int
let float_field to_jv name = prop name to_jv Jv.of_float Jv.to_float

let float_option_field to_jv name : (_, float option) prop =
  ($) (name, to_jv, Option.map_or Jv.of_float ~default:Jv.undefined,
  (fun v -> if Jv.is_undefined v then None else Some (Jv.to_float v)))
let obj_field to_jv name = ($) (name, to_jv, Fun.id, Fun.id)

let obj_option_field to_jv name : (_,_ option) prop =
  ($) (name, to_jv, Option.get_or ~default:Jv.undefined, Option.if_ (Fun.negate Jv.is_undefined))
let obj_array_field to_jv name = ($) (name, to_jv, Jv.of_jv_array, Jv.to_jv_array)

type vector2 = Jv.t
type vector3 = Jv.t
type matrix4 = Jv.t
type skeleton = Jv.t
type object3d = Jv.t
type skinned_mesh = Jv.t
type mesh = Jv.t
type ik = Jv.t
type ik_helper = Jv.t


let three = Jv.get Jv.global "THREE"
let get = Jv.get three
let bl = Jv.of_bool
let fl = Jv.of_float
let int = Jv.of_int
let option f = function None -> Jv.undefined | Some v -> f v

module Vector2 = struct
  type t = Jv.t
  let cls = get "Vector2"
  let to_jv v = v

  let create ?(x=0.) ?(y=0.) () =
    Jv.new' cls [| fl x; fl y |]

  let add v other = ignore @@ Jv.call v "add" [| to_jv other |]

  let x = float_field to_jv "x"
  let y = float_field to_jv "y"

  let set ~x ~y vec =
    ignore @@ Jv.call vec "set" [| fl x; fl y |]

  let get vec =
    let (!) v = Jv.to_float @@ Jv.get vec v in
    ! "x", ! "y"

end

module Vector3 = struct
  type t = Jv.t
  let cls = get "Vector3"
  let to_jv v = v

  let clone t = Jv.call t "clone" [| |]

  let create ?(x=0.) ?(y=0.) ?(z=0.) () =
    Jv.new' cls [| fl x; fl y; fl z |]

  let add v other = ignore @@ Jv.call v "add" [| to_jv other |]

  let set_from_matrix_position v mat =
    ignore @@ Jv.call v "setFromMatrixPosition" [| mat |]

  let add_vectors self a b =
    ignore @@ Jv.call self "addVectors" [| a; b |]

  let add_scalar self scale =
    ignore @@ Jv.call self "addScalar" [| fl scale |]

  let add_scaled_vector self vec scale =
    ignore @@ Jv.call self "addScaledVector" [| vec; fl scale |]

  let cross_vectors self a b =
    ignore @@ Jv.call self "crossVectors" [| a; b |]

  let dot self other =
    Jv.to_float @@ Jv.call self "dot" [| other |]

  let multiply self other =
    ignore @@ Jv.call self "multiply" [| other |]

  let normalise self =
    ignore @@ Jv.call self "normalize" [| |]

  let copy self other =
    (* : t -> t -> unit *)
    ignore @@ Jv.call self "copy" [| other |]

  let angle_to v other = Jv.to_float @@ Jv.call v "angleTo" [| other |]


  let x = float_field to_jv "x"
  let y = float_field to_jv "y"
  let z = float_field to_jv "z"

  let set ~x ~y ~z vec =
    ignore @@ Jv.call vec "set" [| fl x; fl y; fl z |]

  let get vec =
    let (!) v = Jv.to_float @@ Jv.get vec v in
    ! "x", ! "y", ! "z"

  let multiply_scalar vec scalar =
    ignore @@ Jv.call vec "multiplyScalar" [| fl scalar |]

end

module Quaternion = struct
  type t = Jv.t
  let cls = get "Quaternion"
  let to_jv t = t
  let create ?(x=0.) ?(y=0.) ?(z=0.) ?(w=1.) () =
    Jv.new' cls [| fl x; fl y; fl z; fl w |]

  let angle_to = fun self other -> Jv.to_float @@ Jv.call self "angleTo" [| other |]

  let conjugate self =
    (* : t -> t *)
    Jv.call self "conjugate" [| |]

  let copy self other =
    (* : t -> t -> unit *)
    ignore @@ Jv.call self "copy" [| other |]

  let equals self other =
    (* : t -> t -> bool *)
    Jv.to_bool @@ Jv.call self "equals" [| other |]

  let dot self other =
    (*  : t -> t -> float *)
    Jv.to_float @@ Jv.call self "dot" [| other |]

  let identity self =
    (*  : t -> unit *)
    ignore @@ Jv.call self "identity" [| |]

  let length self =
    (* : t -> float *)
    Jv.to_float @@ Jv.call self "length" [| |]

  let normalise self =
    (* : t -> unit *)
    ignore @@ Jv.call self "normalize" [| |]

  let multiply self other =
    (* : t -> t -> unit *)
    ignore @@ Jv.call self "multiply" [| other |]

  let premultiply self other =
    (*  : t -> t -> unit *)
    ignore @@ Jv.call self "premultiply" [| other |]

  let random self =
    (* : t -> unit *)
    ignore @@ Jv.call self "random" [| |]

  let rotate_towards self ~other ~step =
    (* : t -> t -> float -> unit *)
    ignore @@ Jv.call self "rotateTowards" [| other; fl step |]

  let slerp self ~other ~step =
    (* : t -> t -> float -> unit *)
    ignore @@ Jv.call self "slerp" [| other; fl step |]

  let set_from_axis_angle self ~axis ~angle =
    ignore @@ Jv.call self "setFromAxisAngle" [| axis; fl angle |]

  let x = float_field to_jv "x"
  let y = float_field to_jv "y"
  let z = float_field to_jv "z"
  let w = float_field to_jv "w"

end



module Matrix4 = struct
  type t = Jv.t
  let cls = get "Matrix4"
  let to_jv v = v
end


module Color = struct
  type t = Jv.t
  let to_jv v = v

  let cls = get "Color"

  let create_rgb ?(r=0.) ?(g=0.) ?(b=0.) () = Jv.new' cls [| fl r; fl g; fl b |]
  let create_hex hex = Jv.new' cls [| int hex |]
end

module Fog = struct
  type t = Jv.t
  let to_jv v = v

  let cls = get "Fog"

  let create ~color ~near ~far = Jv.new' cls [| int color; fl near; fl far |]

  let color fog = Jv.get fog "color"
end

module Font = struct

  type t = Jv.t

  let to_jv v = v

end

module FontLoader = struct

  type t = Jv.t

  let to_jv v = v
  let cls = get "FontLoader"
  let create () = Jv.new' cls [| |]

  let parse loader json = Jv.call loader "parse" [| json |]

  let load ?on_progress ?on_error loader ~path ~(on_load: Jv.t -> unit) =
    let args = List.flatten [
      [Jv.of_string path; Jv.repr on_load];
      [match on_progress with None -> Jv.null | Some f -> Jv.repr f];
      [match on_error with None -> Jv.null | Some f -> Jv.repr f];
    ] |> Array.of_list in
    ignore @@ Jv.call loader "load" args

end

module Geometry = struct
  type t = Jv.t
  let to_jv (v: t) : Jv.t = v
  let cls_buffer = get "BufferGeometry"
  let cls_box = get "BoxGeometry"
  let cls_sphere = get "SphereGeometry"
  let cls_text = get "TextGeometry"

  let box ?(width=1.) ?(height=1.)  ?(depth=1.)
        ?(width_segments=1) ?(height_segments=1) ?(depth_segments=1) () =
    Jv.new' cls_box [| fl width; fl height; fl depth;
                       int width_segments; int height_segments;
                       int depth_segments |]

  let sphere
        ?(radius=1.)
        ?(width_segments=32) ?(height_segments=16)
        ?(phi_start=0.) ?(phi_length = Float.pi *. 2.)
        ?(theta_start=0.) ?(theta_length=Float.pi) () =
    Jv.new' cls_sphere [|
      fl radius;
      int width_segments; int height_segments;
      fl phi_start; fl phi_length;
      fl theta_start; fl theta_length
    |]

  let text
        ?(size=100.)
        ?(height=50.)
        ?(curve_segments=12)
        ?(bevel_enabled=false)
        ?(bevel_thickness=10.)
        ?(bevel_size=8.)
        ?(bevel_offset=0.)
        ?(bevel_segments=3) font txt =
    Jv.new' cls_text [|
      Jv.of_string txt;
      Jv.obj [|
        "font", Font.to_jv font;
        "size", Jv.of_float size;
        "height", Jv.of_float height;
        "curve_segments", Jv.of_int curve_segments;
        "bevel_enabled", Jv.of_bool bevel_enabled;
        "bevel_thickness", Jv.of_float bevel_thickness;
        "bevel_size", Jv.of_float bevel_size;
        "bevel_offset", Jv.of_float bevel_offset;
        "bevel_segments", Jv.of_int bevel_segments;
      |]
    |]


  let set_from_points buf points =
    ignore @@ Jv.call buf "setFromPoints" 
                [| Jv.of_jv_array points |]

  let buffer ?points () =
    let buf = Jv.new' cls_buffer [| |] in
    begin match points with None -> () | Some points ->
      set_from_points buf points
    end;
    buf

  let bounding_box = obj_field to_jv "boundingBox"

end

module Material = struct
  type t = Jv.t
  let to_jv (v: t) : Jv.t = v

  let cls_mesh_basic = get "MeshBasicMaterial"
  let cls_line_basic = get "LineBasicMaterial"

  let mesh_basic ?color () =
    let options = List.flatten [
      match color with None -> [] | Some color ->
        ["color", Color.to_jv color]
    ] |> Array.of_list |> Jv.obj in
    Jv.new' cls_mesh_basic [| options |]

  let line_basic ?color () =
    let options = List.flatten [
      match color with None -> [] | Some color ->
        ["color", Color.to_jv color]
    ] |> Array.of_list |> Jv.obj in
    Jv.new' cls_line_basic [| options |]

  let wireframe = bool_field to_jv "wireframe"

  let side : (t, _) prop =
    let side_to_jv = function
      | `Front -> get "FrontSide"
      | `Back -> get "BackSide"
      | `Double -> get "DoubleSide" in
    let jv_to_side = function
      | jv when Jv.equal jv (get "FrontSide") -> `Front
      | jv when Jv.equal jv (get "BackSide") -> `Back
      | _ -> `Double in
    ($) ("side", to_jv, side_to_jv, jv_to_side)
end

module Box3 = struct
  type t = Jv.t
  let to_jv t = t

  let min = obj_field to_jv "min"
  let max = obj_field to_jv "max"

  let set_from_object v obj =
    ignore @@ Jv.call v "setFromObject" [| obj |]
end

module Mesh = struct
  type t = Jv.t
  let to_jv (v: t) : Jv.t = v
  let upcast (v: t) : t = v
  let cls = get "Mesh"
  let create ~geometry ~material =
    Jv.new' cls [| Geometry.to_jv geometry ; Material.to_jv material |]

end

module Scene = struct
  type t = Jv.t
  let to_jv v = v

  let cls = get "Scene"

  let create () = Jv.new' cls [| |]

  let set_background scene bg = Jv.set scene "background" (Color.to_jv bg)

  let set_fog scene fog = Jv.set scene "fog" (Fog.to_jv fog)
  let add scene obj = ignore @@ Jv.call scene "add" [| obj |]

  let children = obj_array_field to_jv "children"
end

module PerspectiveCamera = struct
  type t = Jv.t
  let cls = get "PerspectiveCamera"
  let to_jv v = v
  let create ~fov ~aspect ~near ~far =
    Jv.new' cls [| Jv.of_float fov; Jv.of_float aspect; Jv.of_float near; Jv.of_float far |]

  let position camera = Jv.get camera "position"

  let set_aspect camera aspect = Jv.set camera "aspect" (fl aspect)

  let update_projection_matrix camera =
    ignore @@ Jv.call camera "updateProjectionMatrix" [| |]

  let update_matrix_world camera =
    ignore @@ Jv.call camera "updateMatrixWorld" [| |]

end

module OrbitControls = struct
  type t = Jv.t
  let cls = get "OrbitControls"
  let to_jv (v: t) : Jv.t = v

  let create ~camera ~input_source =
    Jv.new' cls [| PerspectiveCamera.to_jv camera; El.to_jv input_source |]

  let auto_rotate = bool_field to_jv "autoRotate"

  let enabled = bool_field to_jv "enabled"
  let enable_pan = bool_field to_jv "enablePan"

  let target = obj_field to_jv "target"

  let update camera = ignore @@ Jv.call camera "update" [| |]

end

module TransformControls = struct

  type t = Jv.t
  let cls = get "TransformControls"
  let to_jv v = v

  let create ~camera ~element =
    Jv.new' cls [| PerspectiveCamera.to_jv camera; El.to_jv element |]

  let camera = obj_field to_jv "camera"
  let enabled = bool_field to_jv "enabled"

  let mode : (t, [ `Rotate | `Scale | `Translate ]) prop =
    let mode_of_jv jv = match Jv.to_string jv with
      | "translate" -> `Translate
      | "rotate" -> `Rotate
      | "scale" -> `Scale
      | _ -> failwith "unknown mode" in
    let jv_of_mode = function
      | `Translate -> Jv.of_string "translate"
      | `Rotate -> Jv.of_string "rotate"
      | `Scale -> Jv.of_string "scale" in
    ($) ("mode", to_jv, jv_of_mode, mode_of_jv)

  let attach cntr obj =
    ignore @@ Jv.call cntr "attach" [| obj |]

  let detach cntr =
    ignore @@ Jv.call cntr "detach" [| |]

end

module CSS2DRenderer = struct

  type t = Jv.t
  let cls = get "CSS2DRenderer"
  let to_jv v = v

  let create ?element () = Jv.new' cls (match element with None -> [| |] | Some el -> [| El.to_jv el |])

  let get_size v =
    let obj = Jv.call v "getSize" [| |] in
    Jv.to_float (Jv.get obj "width"),
    Jv.to_float (Jv.get obj "height")

  let render renderer ~scene ~camera =
    ignore @@ Jv.call renderer "render" [| scene; camera |]
  
  let set_size renderer ~width ~height =
    ignore @@ Jv.call renderer "setSize" [| fl width; fl height |]

  let dom_element v = El.of_jv (Jv.get v "domElement")
end

module WebGLRenderer = struct
  type t = Jv.t
  let cls = get "WebGLRenderer"
  let to_jv v = v

  let create ?canvas ?alpha ?antialias () =
    let params =
      List.flatten [
        (match canvas with None -> [] | Some elt -> ["canvas", El.to_jv elt]);
        (match alpha with None -> [] | Some elt -> ["alpha", bl elt]);
        (match antialias with None -> [] | Some elt -> ["antialias", bl elt]);
      ] |> Array.of_list in
    Jv.new' cls [| Jv.obj params |]

  let clear ?(color=true) ?(depth=true) ?(stencil=true)  renderer () =
    ignore @@ Jv.call renderer "clear" [| bl color; bl depth; bl stencil |]

  let compile renderer ~scene ~camera =
    ignore @@ Jv.call renderer "compiler" [| Scene.to_jv scene; PerspectiveCamera.to_jv camera |]

  let render renderer ~scene ~camera =
    ignore @@ Jv.call renderer "render" [| Scene.to_jv scene; PerspectiveCamera.to_jv camera |]

  let set_pixel_ratio renderer ratio =
    ignore @@ Jv.call renderer "setPixelRatio" [| fl ratio |]

  let set_size ?(update_style=false) renderer ~width ~height =
    ignore @@ Jv.call renderer "setSize" [| fl width; fl height; bl update_style |]

  let dom_element v = El.of_jv (Jv.get v "domElement")

end


module SkinnedMesh = struct
  type t = Jv.t
  let to_jv v = v
  let material = obj_field Fun.id "material"
  let skeleton v = Jv.get v "skeleton"
  let bind v skeleton = ignore @@ Jv.call v "bind" [| skeleton |]
  let pose v = ignore @@ Jv.call v "pose" [| |]
  let upcast v = v
end

module Skeleton = struct
  type t = Jv.t
  let bones v = Jv.to_jv_array @@ Jv.get v "bones"
  let to_jv v = v
end

module SkeletonHelper = struct
  type t = Jv.t
  let cls = get "SkeletonHelper"
  let to_jv v = v
  let create mesh =
    Jv.new' cls [| mesh |]
end


module Bone = struct
  type t = Jv.t
  let to_jv v = v
  let upcast v = v
end

module Line = struct
  type t = Jv.t
  let to_jv v = v

  let cls = get "Line"

  let create ~geometry ~material =
    Jv.new' cls  [| Geometry.to_jv geometry; Material.to_jv material |]

  let upcast v = v

end

module CSS2DObject = struct
  type t = Jv.t
  let cls = get"CSS2DObject"
  let to_jv v = v
  let upcast t = t
  let create el = Jv.new' cls [| El.to_jv el |]
  let element el = El.of_jv (Jv.get el "element")
end


module Object3D = struct
  type t = Jv.t
  let to_jv (v: t) : Jv.t = v


  let position = obj_field to_jv "position"

  let geometry = obj_field to_jv "geometry"

  let quaternion = obj_field to_jv "quaternion"

  let add v child = ignore @@ Jv.call v "add" [| child |]

  let traverse v fn = ignore @@ Jv.call v "traverse" [| Jv.repr fn |]

  let children v  = Jv.to_jv_array @@ Jv.get v "children"

  let parent v = Jv.find v "parent"


  let id = string_field to_jv "id"

  let name = string_field to_jv "name"

  let world_to_local self vec = ignore @@ Jv.call self "worldToLocal" [| vec |]

  let get_world_position self target = ignore @@ Jv.call self "getWorldPosition" [| target |]

  let update_matrix_world ?force self =
    ignore @@ Jv.call self "updateMatrixWorld"
                (match force with None -> [| |] | Some v -> [| Jv.of_bool v |])


  let ty v = match Jv.to_string @@ Jv.get v "type" with
    | "Group" -> `Group
    | "CSS2DObject" -> `CSS2DObject
    | "Object3D" -> `Object3D
    | "Bone" -> `Bone
    | "SkinnedMesh" -> `SkinnedMesh
    | "Mesh" -> `Mesh
    | "Line" -> `Line
    | "IKHelper" -> `IKHelper
    | err -> failwith ("unsupported type" ^ err)

  let downcast v = match Jv.to_string @@ Jv.get v "type" with
    | "Group" -> `Group
    | "CSS2DObject" -> `CSS2DObject v
    | "Object3D" -> `Object3D v
    | "Bone" -> `Bone v
    | "SkinnedMesh" -> `SkinnedMesh v
    | "Mesh" -> `Mesh v
    | "Line" -> `Line v
    | "IKHelper" -> `IKHelper v
    | err -> failwith ("unsupported type" ^ err)
end

module CCDIKSolverHelper = struct
  type t = Jv.t
  let to_jv (v: t) : Jv.t = v
end


module IKLink = struct

  type t = Jv.t 
  let to_jv (v: t) : Jv.t = v

  let create ?limitation ?rotation_minimum ?rotation_maximum ?(enabled=true) index =
    Jv.obj (List.flatten [
      ["index", Jv.of_int index];
      ["enabled", Jv.of_bool enabled];
      (match limitation with None -> [] | Some v -> ["limitation", v]);
      (match rotation_minimum with None -> [] | Some v -> ["rotationMin", Jv.of_float v]);
      (match rotation_maximum with None -> [] | Some v -> ["rotationMax", Jv.of_float v]);
    ] |> Array.of_list)

  let index = int_field to_jv "index"

  let limitation = obj_option_field to_jv "limitation"

  let rotation_min = obj_option_field to_jv "rotationMin"

  let rotation_max = obj_option_field to_jv "rotationMax"

  let enabled = bool_field to_jv "enabled"
  
end

module IKParameter = struct

  type t = Jv.t 
  let to_jv (v: t) : Jv.t = v

  let create ?(iteration=1) ?min_angle ?max_angle ~target ~effector ~links () =
    Jv.obj (List.flatten [
      ["target", Jv.of_int target];
      ["effector", Jv.of_int effector];
      ["links", Jv.of_jv_array links];
      ["iteration", Jv.of_int iteration];
      (match min_angle with None -> [] | Some min_angle -> ["minAngle", Jv.of_float min_angle]);
      (match max_angle with None -> [] | Some max_angle -> ["maxAngle", Jv.of_float max_angle]);
    ] |> Array.of_list)

  let target = int_field to_jv "target"
  let effector = int_field to_jv "effector"
  let links = obj_array_field to_jv "links"
  let iteration = int_field to_jv "iteration"
  let min_angle = float_option_field to_jv "minAngle"
  let max_angle = float_option_field to_jv "maxAngle"

end

module CCDIKSolver = struct

  type t = Jv.t
  let to_jv (v: t) : Jv.t = v
  let cls = get "CCDIKSolver"

  let create ~mesh ~iks = Jv.new' cls [| SkinnedMesh.to_jv mesh; Jv.of_jv_array iks |]

  let create_helper solver = Jv.call solver "createHelper" [| |]

  let update solver = ignore @@ Jv.call solver "update" [| |]

end



module GLTF = struct

  type t = Jv.t
  let to_jv (v: t) : Jv.t = v

  let scene v : Jv.t = Jv.get v "scene"

end

module GLTFLoader = struct
  type t = Jv.t
  let cls = get "GLTFLoader"
  let to_jv (v: t) : Jv.t = v

  let create () = Jv.new' cls [| |]

  let load ?on_progress ?on_error loader ~path ~on_load =
    let args = List.flatten [
      [Jv.of_string path; Jv.repr on_load];
      [match on_progress with None -> Jv.null | Some f -> Jv.repr f];
      [match on_error with None -> Jv.null | Some f -> Jv.repr f];
    ] |> Array.of_list in
    ignore @@ Jv.call loader "load" args
end

module PolarGridHelper = struct

  type t = Jv.t
  let to_jv v = v
  let cls = get "PolarGridHelper"

  let create ?(radius=10.) ?(radials=16) ?(circles=8) ?(divisions=64) ?color1 ?color2 () =
    Jv.new' cls [|
      fl radius;
      int radials;
      int circles;
      int divisions;
      option Color.to_jv color1;
      option Color.to_jv color2;
    |]

end

module Intersection = struct
  type t = Jv.t
  let to_jv = fun v -> v
  let distance = float_field to_jv "distance"
  let point = obj_field to_jv "point"
  let obj = obj_field to_jv "object"
  let uv = obj_field to_jv "uv"
  let uv2 = obj_field to_jv "uv2"
  let instance_id = int_field to_jv "instanceId"
end

module Ray = struct

  type t = Jv.t
  let to_jv v = v
  let cls = get "Ray"

  let create ?origin ?direction () =
    Jv.new' cls [|
      option Vector3.to_jv origin;
      option Vector3.to_jv direction;
    |]

  let origin = obj_field to_jv "origin"
  let direction = obj_field to_jv "direction"

end

module Raycaster = struct

  type t = Jv.t
  let to_jv v = v
  let cls = get "Raycaster"

  let create ?origin ?direction ?near ?far () =
    Jv.new' cls [|
      Option.map_or ~default:(Vector3.create ()) Vector3.to_jv origin;
      Option.map_or ~default:(Vector3.create ~z:(-.1.) ()) Vector3.to_jv direction;
      option Jv.of_float near;
      option Jv.of_float far;
    |]

  let far = float_field to_jv "far"
  let near = float_field to_jv "near"
  let camera = obj_field to_jv "camera"

  let set raycaster ~origin ~direction =
    ignore @@ Jv.call raycaster "set" [|
      Vector3.to_jv origin;
      Vector3.to_jv direction;
    |]

  let set_from_camera raycaster ~coords ~camera =
    ignore @@ Jv.call raycaster "setFromCamera" [|
      Vector2.to_jv coords;
      PerspectiveCamera.to_jv camera
    |]

  let intersect_object ?(recursive=true) raycaster obj =
    Jv.to_jv_array @@ Jv.call raycaster "intersectObject" [|
      Object3D.to_jv obj;
      Jv.of_bool recursive
    |]
    
  let intersect_objects ?(recursive=true) raycaster obj =
    Jv.to_jv_array @@ Jv.call raycaster "intersectObjects" [|
      Jv.of_jv_array obj;
      Jv.of_bool recursive
    |]

  let ray = obj_field to_jv "ray"

end


module IK = struct
  module Constraint = struct

    type t = Jv.t

    let cls_ball = get "IKBallConstraint"
    let to_jv v = v

    let ball fl = Jv.new' cls_ball [| Jv.of_float fl |]

  end

  module Joint = struct

    type t = Jv.t

    let cls = get "IKJoint"

    let to_jv v = v

    let create
          ~bone ~constraints =
      Jv.new' cls [|
        bone;
        Jv.obj [|
          "constraints", Jv.of_jv_array constraints
        |]
      |]

  end

  module Chain = struct

    type t = Jv.t

    let cls = get "IKChain"

    let to_jv v = v

    let create () = Jv.new' cls [| |]

    let add ?target t joint  =
      ignore @@ Jv.call t "add" (List.flatten [
        [Joint.to_jv joint];
        match target with
          None -> []
        | Some target ->
          [Jv.obj [|
            "target", Object3D.to_jv target
          |]]
      ] |> Array.of_list)


    let connect chain other =
      ignore @@ Jv.call chain "connect" [| other |]

  end

  module Helper = struct

    type t = Jv.t

    let cls = get "IKHelper"

    let upcast v = v

    let to_jv v = v

    let create
          ?color ?show_bones ?show_axes
          ?wireframe ?axes_size ?bone_size
          t =
      let option str f v = match v with None -> [] | Some v -> [str, f v] in
      let config = List.flatten [
        option "color" Color.to_jv color;
        option "showBones" Jv.of_bool show_bones;
        option "showAxes" Jv.of_bool show_axes;
        option "wireframe" Jv.of_bool wireframe;
        option "axesSize" Jv.of_float axes_size;
        option "boneSize" Jv.of_float bone_size;
      ] |> Array.of_list |> Jv.obj in
      Jv.new' cls [| t; config |]

  end

  type t = Jv.t

  let cls = get "IK"

  let to_jv v = v

  let create () = Jv.new' cls [| |]


  let add system chain =
    ignore @@ Jv.call system "add" [|
      Chain.to_jv chain
    |]

  let root_bone system = Jv.call system "getRootBone" [| |]

  let solve system = ignore @@ Jv.call system "solve" [| |]

  let recalculate system = ignore @@ Jv.call system "recalculate" [| |]

end
