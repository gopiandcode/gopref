open Brr

type ('b,'a) prop = string * ('b -> Jv.t) * ('a -> Jv.t) * (Jv.t -> 'a)

let (.@[] <-) obj ((name, obj_to_jv, to_jv, _): ('b,'a) prop) (v: 'a) = Jv.set (obj_to_jv obj) name (to_jv v)
let (.@[]) obj ((name, obj_to_jv, _, from_jv): ('b,'a) prop) = from_jv (Jv.get (obj_to_jv obj) name)
let prop name to_jv from_ to_ : ('b,'a) prop = (name, to_jv, from_, to_)

module ResizeObserver : sig
  type t
  val create : (Jv.t array -> t -> unit) -> t
  val observe :
    ?box:[< `BorderBox | `ContentBox | `DevicePixelContentBox ] ->
    t -> El.t -> unit
end = struct

  type t = Jv.t

  let cls = Jv.get Jv.global "ResizeObserver"

  let create (fn: Jv.t array -> t -> unit) =
    Jv.new' cls [| Jv.repr fn |]

  let observe ?box observer target =
    let args =
      List.flatten [
        [El.to_jv target];
        match box with
          None -> []
        | Some v ->
          let cls = match v with
            | `ContentBox -> "content-box"
            | `BorderBox -> "border-box"
            | `DevicePixelContentBox -> "device-pixel-content-box" in
          [Jv.obj [| "box", Jv.of_string cls |]]
      ] |> Array.of_list in
    ignore @@ Jv.call observer "observe" args

end
