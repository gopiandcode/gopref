open Brr

type ('b,'a) prop
val ( .@[]<- ) : 'b -> ('b,'a) prop -> 'a -> unit
val ( .@[] ) : 'b -> ('b,'a) prop -> 'a
val prop: string -> ('b -> Jv.t) -> ('a -> Jv.t) -> (Jv.t -> 'a) -> ('b, 'a) prop

module ResizeObserver : sig
  type t
  val create : (Jv.t array -> t -> unit) -> t
  val observe :
    ?box:[< `BorderBox | `ContentBox | `DevicePixelContentBox ] ->
    t -> El.t -> unit
end
