open Containers
open Three_int
open Utils

let bone_children (v: Bone.t) =
  Object3D.children (v :> Object3D.t)
  |> Array.to_iter
  |> Iter.filter_map (fun v -> match Object3D.downcast v with
    | `Bone v -> Some v
    | _ -> None
  )

let bone_id (bone: Bone.t) =
  ((bone :> Object3D.t).@[Object3D.id])

let reset_quat = Quaternion.create ()
let forward = Vector3.create ~x:0. ~y:0. ~z:1. ()

let get_alignment_quaternion =
  let t = Vector3.create () in
  let q = Quaternion.create () in
  fun from_dir to_dir ->
    Vector3.cross_vectors t from_dir to_dir;
    Vector3.normalise t;
    let adjust_angle = Vector3.angle_to from_dir to_dir in
    if not @@ Float.equal adjust_angle 0.0 then begin
      Quaternion.set_from_axis_angle q ~axis:t ~angle:adjust_angle;
      Some q
    end
    else None

let rec update_transformations (parent_bone: Bone.t) world_pos =
  Brr.Console.(log  [
    Jstr.of_string "updating transforms of ";
    Jstr.of_string  (parent_bone :> Object3D.t).@[Object3D.name]]);
  let lookup_pos (bone: Bone.t) =
    Hashtbl.find world_pos (bone_id bone) in
  let average_dir = Vector3.create () in
  let count = ref 0 in
  let () =
    bone_children parent_bone |>
    Iter.iter (fun (bone: Bone.t) ->
      incr count;
      let child_pos = lookup_pos bone in
      Vector3.add average_dir child_pos
    ) in

  let is_stolmach parent_bone =
    Jstr.starts_with ~prefix:(Jstr.of_string "Stolmach")
      (Jv.to_jstr (Jv.get (Bone.to_jv parent_bone) "name")) in
  let _is_special_cased parent_bone =
    let name = (Jv.to_jstr (Jv.get (Bone.to_jv parent_bone) "name")) in
    Jstr.starts_with ~prefix:(Jstr.of_string "Pelvis") name ||
    Jstr.starts_with ~prefix:(Jstr.of_string "Chest") name in

  if !count > 0 then begin

    Vector3.multiply_scalar average_dir
      (1./. (Float.of_int !count));


    Quaternion.copy (parent_bone :> Object3D.t).@[Object3D.quaternion] reset_quat;
    Object3D.update_matrix_world (parent_bone :> Object3D.t);

    let child_bone_dir =
      Object3D.world_to_local (parent_bone :> Object3D.t) average_dir;
      Vector3.normalise average_dir;
      average_dir in


    let quat = get_alignment_quaternion forward child_bone_dir in
    if Option.is_none quat  then begin
      Brr.Console.(log [
        parent_bone;
        str "get_alignment_quaternion ";
        forward;
        child_bone_dir;
        str " = ";
        quat
      ]);
    end;
    (* let quat = if Option.is_none quat && is_special_cased parent_bone then begin
     *   Brr.Console.(log [
     *     str "get_alignment_quaternion ";
     *     forward;
     *     child_bone_dir;
     *     str " = ";
     *     quat
     *   ]);
     *   let q = Quaternion.create () in
     *   Quaternion.set_from_axis_angle q ~axis:(Vector3.create ~x:1. ()) ~angle:(Float.pi /. 2.);
     *   Some q
     * end else quat in *)




    begin match quat with None -> () | Some quat ->

      Quaternion.premultiply (parent_bone :> Object3D.t).@[Object3D.quaternion] quat;

      Object3D.update_matrix_world (parent_bone :> Object3D.t);

      bone_children parent_bone (fun child_bone ->
        if is_stolmach child_bone then
          Brr.Console.(log [
            str "child is stolmach, and parent is";
            parent_bone
          ]);
        let child_bone_pos_world = lookup_pos child_bone |> Vector3.clone in
        Object3D.world_to_local (parent_bone :> Object3D.t) child_bone_pos_world;
        if not (is_stolmach child_bone) then
          Vector3.copy (child_bone :> Object3D.t).@[(Object3D.position)] child_bone_pos_world;
      ) 
    end;

    bone_children parent_bone (fun child_bone ->
      if is_stolmach child_bone then
        Brr.Console.(log [
          str "child is stolmach, and parent is";
          parent_bone
        ]);
      update_transformations child_bone world_pos
    )
  end

let rec get_original_world_positions root_bone world_pos =
  bone_children root_bone (fun child ->
    let child_world_pos = Vector3.create () in
    Object3D.get_world_position (child :> Object3D.t) child_world_pos;
    Hashtbl.add world_pos (bone_id child) child_world_pos;
    get_original_world_positions child world_pos
  )

let set_z_forward root_bone =
  let world_pos = Hashtbl.create 10 in
  get_original_world_positions root_bone world_pos;
  update_transformations root_bone world_pos
