open Brr
open Utils

type vector2
type vector3 
type matrix4
type skeleton
type object3d
type skinned_mesh = private object3d
type mesh
type ik
type ik_helper = private object3d

module Vector2 : sig
  type t = vector2
  val to_jv : t -> Jv.t
  val create : ?x:float -> ?y:float -> unit -> t
  val add : t -> t -> unit
  val set : x:float -> y:float -> t -> unit
  val get : t -> float * float

  val x : (t,float) prop
  val y : (t,float) prop
end

module Vector3 : sig
  type t = vector3
  val to_jv : t -> Jv.t
  val create : ?x:float -> ?y:float -> ?z:float -> unit -> t
  val clone : t -> t
  val add : t -> t -> unit
  val add_scalar : t -> float -> unit
  val add_vectors : t -> t -> t -> unit
  val add_scaled_vector : t -> t -> float -> unit
  val multiply: t -> t -> unit
  val multiply_scalar: t -> float -> unit
  val cross_vectors : t -> t -> t -> unit
  val dot: t -> t -> float
  val normalise: t -> unit
  val copy: t -> t -> unit
  val angle_to: t -> t -> float
  val set_from_matrix_position : t -> matrix4 -> unit
  val set : x:float -> y:float -> z:float -> t -> unit
  val get : t -> float * float * float

  val x : (t,float) prop
  val y : (t,float) prop
  val z : (t,float) prop
end

module Quaternion : sig
  type t
  val to_jv : t -> Jv.t
  val create : ?x:float -> ?y:float -> ?z:float -> ?w:float -> unit -> t
  val angle_to: t -> t -> float
  val conjugate: t -> t
  val copy: t -> t -> unit
  val equals: t -> t -> bool
  val dot : t -> t -> float
  val identity : t -> unit
  val length: t -> float
  val normalise: t -> unit
  val multiply: t -> t -> unit
  val premultiply : t -> t -> unit
  val random: t -> unit
  val rotate_towards: t -> other:t -> step:float -> unit
  val slerp: t -> other:t -> step:float -> unit
  val set_from_axis_angle: t -> axis:Vector3.t -> angle:float -> unit

  val x : (t,float) prop
  val y : (t,float) prop
  val z : (t,float) prop
  val w : (t,float) prop
end


module Matrix4 : sig
  type t = matrix4
  val cls : t
  val to_jv : t -> Jv.t
end

module Color : sig
  type t
  val to_jv : t -> Jv.t
  val create_rgb : ?r:float -> ?g:float -> ?b:float -> unit -> t
  val create_hex : int -> t
end

module Fog : sig
  type t
  val to_jv : t -> Jv.t
  val create : color:int -> near:float -> far:float -> t
  val color : t -> Color.t
end

module Box3 : sig
  type t
  val to_jv : t -> Jv.t
  val min: (t, Vector3.t) prop
  val max: (t, Vector3.t) prop
  val set_from_object: t -> object3d -> unit
end

module Font : sig
  type t
  val to_jv: t -> Jv.t
end

module FontLoader : sig
  type t
  val to_jv: t -> Jv.t
  val create: unit -> t
  val load :
    ?on_progress:(total:int -> loaded:int -> unit) ->
    ?on_error:(Jv.t -> unit) -> t -> path:string -> on_load:(Font.t -> unit) -> unit
  val parse: t -> Jv.t -> Font.t
end

module Geometry : sig
  type t
  val to_jv : t -> Jv.t
  val box :
    ?width:float ->
    ?height:float ->
    ?depth:float ->
    ?width_segments:int ->
    ?height_segments:int -> ?depth_segments:int -> unit -> t
  val sphere :
    ?radius:float ->
    ?width_segments:int ->
    ?height_segments:int ->
    ?phi_start:float ->
    ?phi_length:float ->
    ?theta_start:float -> ?theta_length:float -> unit -> t

  val text:
    ?size:float ->
    ?height:float ->
    ?curve_segments:int ->
    ?bevel_enabled:bool ->
    ?bevel_thickness:float ->
    ?bevel_size:float ->
    ?bevel_offset:float ->
    ?bevel_segments:int ->
    Font.t -> string -> t

  val buffer : ?points:Vector3.t array -> unit -> t

  val set_from_points : t -> Vector3.t array -> unit

  val bounding_box: (t, Box3.t) prop
end

module Material : sig
  type t
  val to_jv : t -> Jv.t
  val cls_mesh_basic : t
  val mesh_basic : ?color:Color.t -> unit -> t
  val line_basic : ?color:Color.t -> unit -> t
  val wireframe : (t, bool) prop
  val side: (t, [ `Back | `Double | `Front ]) prop
end

module Scene : sig
  type t
  val to_jv : t -> Jv.t
  val create : unit -> t
  val set_background : t -> Color.t -> unit
  val set_fog : t -> Fog.t -> unit
  val add : t -> Jv.t -> unit
  val children: (t, object3d array) prop
end

module PerspectiveCamera : sig
  type t
  val create : fov:float -> aspect:float -> near:float -> far:float -> t
  val position : t -> Vector3.t
  val set_aspect : t -> float -> unit
  val update_projection_matrix : t -> unit
  val update_matrix_world : t -> unit
  val to_jv : t -> Jv.t
end

module OrbitControls : sig
  type t
  val cls : t
  val to_jv : t -> Jv.t
  val create : camera:PerspectiveCamera.t -> input_source:El.t -> t
  val auto_rotate : (t, bool) prop
  val enabled : (t, bool) prop
  val enable_pan : (t, bool) prop
  val target : (t, Vector3.t) prop
  val update : t -> unit
end

module TransformControls : sig
  type t
  val to_jv : t -> Jv.t
  val create : camera:PerspectiveCamera.t -> element:El.t -> t
  val camera : (t, PerspectiveCamera.t) prop
  val enabled : (t, bool) prop
  val mode : (t, [ `Rotate | `Scale | `Translate ]) prop
  val attach : t -> object3d -> unit
  val detach : t -> unit
end

module CSS2DRenderer: sig
  type t
  val to_jv : t -> Jv.t
  val dom_element: t -> El.t
  val create :  ?element:El.t -> unit -> t
  val get_size : t -> float * float
  val render : t -> scene:Scene.t -> camera:PerspectiveCamera.t -> unit
  val set_size : t -> width:float -> height:float -> unit
end

module WebGLRenderer : sig
  type t
  val to_jv : t -> Jv.t
  val create :
    ?canvas:El.t -> ?alpha:bool -> ?antialias:bool -> unit -> t
  val clear :
    ?color:bool -> ?depth:bool -> ?stencil:bool -> t -> unit -> unit
  val compile : t -> scene:Scene.t -> camera:PerspectiveCamera.t -> unit
  val render : t -> scene:Scene.t -> camera:PerspectiveCamera.t -> unit

  val set_pixel_ratio : t -> float -> unit

  val set_size :
    ?update_style:bool -> t -> width:float -> height:float -> unit

  val dom_element: t -> El.t
end

module SkinnedMesh : sig
  type t = skinned_mesh
  val material: (t, Material.t) prop
  val skeleton : t -> skeleton
  val bind : t -> skeleton -> unit
  val pose: t -> unit
end

module SkeletonHelper : sig
  type t
  val to_jv: t -> Jv.t
  val create : object3d -> t
end

module Bone : sig
  type t = private object3d
  val to_jv : t -> Jv.t
  val upcast : t -> object3d
end

module Skeleton : sig
  type t = skeleton
  val bones : t -> Bone.t array
  val to_jv : t -> Jv.t
end


module Mesh : sig
  type t = private object3d
  val to_jv : t -> Jv.t
  val create : geometry:Geometry.t -> material:Material.t -> t
end

module Line : sig
  type t = private object3d
  val to_jv : t -> Jv.t
  val create : geometry:Geometry.t -> material:Material.t -> t
end

module CSS2DObject : sig
  type t = private object3d
  val to_jv: t -> Jv.t
  val create : El.t -> t
  val element : t -> El.t
end

module Object3D : sig
  type t = object3d
  val add : t -> t -> unit
  val to_jv : t -> Jv.t
  val traverse : t -> (t -> unit) -> unit
  val world_to_local: t -> Vector3.t -> unit
  val get_world_position: t -> Vector3.t -> unit
  val update_matrix_world : ?force:bool -> t -> unit

  val children : t -> t array
  val id : (t,string) prop
  val name : (t,string) prop
  val parent : t -> t option

  val ty : t -> [> `Bone | `Group | `Mesh | `Object3D | `SkinnedMesh | `Line | `IKHelper | `CSS2DObject ]


  val position : (t,Vector3.t) prop
  val geometry : (t,Geometry.t) prop
  val quaternion: (t, Quaternion.t) prop

  val downcast :
    t ->
    [> `Bone of Bone.t
    | `Group
    | `Mesh of Mesh.t
    | `IKHelper of ik_helper
    | `Object3D of t
    | `Line of Line.t
    | `SkinnedMesh of SkinnedMesh.t
    | `CSS2DObject of CSS2DObject.t
    ]
end

module IKLink : sig
  type t
  val to_jv : t -> Jv.t
  val create :
    ?limitation:Vector3.t ->
    ?rotation_minimum:float ->
    ?rotation_maximum:float -> ?enabled:bool -> int -> t
  val index : (t, int) prop
  val limitation : (t, Vector3.t option) prop
  val rotation_min : (t, Vector3.t option) prop
  val rotation_max : (t, Vector3.t option) prop
  val enabled : (t, bool) prop
end

module IKParameter : sig
  type t
  val to_jv : t -> Jv.t
  val create :
    ?iteration:int ->
    ?min_angle:float ->
    ?max_angle:float ->
    target:int -> effector:int -> links:IKLink.t array -> unit -> t

  val target : (t, int) prop

  val effector : (t, int) prop

  val links : (t, IKLink.t array) prop

  val iteration : (t, int) prop

  val min_angle : (t, float option) prop

  val max_angle : (t, float option) prop

end

module CCDIKSolverHelper : sig
  type t
  val to_jv : t -> Jv.t
end

module CCDIKSolver : sig
  type t
  val to_jv : t -> Jv.t
  val create : mesh:SkinnedMesh.t -> iks:IKParameter.t array -> t
  val create_helper : t -> CCDIKSolverHelper.t
  val update : t -> unit
end

module GLTF : sig
  type t
  val to_jv : t -> Jv.t
  val scene : t -> Object3D.t
end

module GLTFLoader : sig
  type t
  val cls : t
  val to_jv : t -> Jv.t
  val create : unit -> t
  val load :
    ?on_progress:(total:int -> loaded:int -> unit) ->
    ?on_error:(Jv.t -> unit) -> t -> path:string -> on_load:(GLTF.t -> unit) -> unit
end

module PolarGridHelper : sig
  type t
  val to_jv : t -> Jv.t
  val create :
    ?radius:float ->
    ?radials:int ->
    ?circles:int -> ?divisions:int -> ?color1:Color.t -> ?color2:Color.t -> unit -> t
end

module Intersection : sig
  type t
  val to_jv: t -> Jv.t
  val distance: (t, float) prop
  val point: (t, Vector3.t) prop
  val obj: (t, Object3D.t) prop
  val uv: (t, Vector2.t) prop
  val uv2: (t, Vector2.t) prop
  val instance_id: (t, int) prop
end

module Ray : sig
  type t
  val to_jv : t -> Jv.t
  val create : ?origin:Vector3.t -> ?direction:Vector3.t -> unit -> t
  val origin : (t, Vector3.t) prop
  val direction : (t, Vector3.t) prop
end


module Raycaster : sig
  type t
  val to_jv : t -> Jv.t
  val create : ?origin:Vector3.t -> ?direction:Vector3.t -> ?near:float -> ?far:float -> unit -> t
  val far : (t, float) prop
  val near : (t, float) prop
  val camera : (t, PerspectiveCamera.t) prop

  val set: t -> origin:Vector3.t -> direction:Vector3.t -> unit
  val set_from_camera: t -> coords:Vector2.t -> camera:PerspectiveCamera.t -> unit
  val intersect_object: ?recursive:bool -> t -> Object3D.t -> Intersection.t array
  val intersect_objects: ?recursive:bool -> t -> Object3D.t array -> Intersection.t array

  val ray: (t, Ray.t) prop
end


module IK : sig

  module Constraint : sig

    type t

    val to_jv: t -> Jv.t

    val ball : float -> t

  end

  module Joint : sig

    type t

    val to_jv: t -> Jv.t

    val create: bone:Bone.t -> constraints:Constraint.t array -> t

  end

  module Chain : sig

    type t

    val to_jv: t -> Jv.t

    val create : unit -> t

    val add: ?target:Object3D.t -> t -> Joint.t -> unit

    val connect: t -> t -> unit

  end

  module Helper : sig

    type t = ik_helper

    val to_jv: t -> Jv.t

    val create:
      ?color:Color.t ->
      ?show_bones:bool ->
      ?show_axes:bool ->
      ?wireframe:bool ->
      ?axes_size:float ->
      ?bone_size:float ->
      ik -> t

  end


  type t = ik

  val to_jv: t -> Jv.t

  val create : unit -> t

  val add : t -> Chain.t -> unit

  val root_bone: t -> Bone.t

  val solve: t -> unit

  val recalculate : t -> unit

end
