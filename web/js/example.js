function Example () {
     const background_color = 0xf1f1f1;

     const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
     camera.position.z = 30 
     camera.position.x = 0;
     camera.position.y = -3;
     const canvas = document.querySelector("#c");

     const scene = new THREE.Scene();
     scene.background = new THREE.Color(background_color);
     scene.fog = new THREE.Fog(background_color, 60, 100);

     const renderer = new THREE.WebGLRenderer({canvas, antialias:true});
     renderer.shadowMap.enabled = true;
     renderer.setPixelRatio(window.devicePixelRatio);
     renderer.setSize( window.innerWidth/2, window.innerHeight/2 );


     let floorGeometry = new THREE.PlaneGeometry(5000, 5000, 1, 1);
     let floorMaterial = new THREE.MeshPhongMaterial({
       color: 0xeeeeee,
       shininess: 0,
     });

     let floor = new THREE.Mesh(floorGeometry, floorMaterial);
     floor.rotation.x = -0.5 * Math.PI; // This is 90 degrees by the way
     floor.receiveShadow = true;
     floor.position.y = -11;
     scene.add(floor);
     

     /* CUBE start */
     const geometry = new THREE.BoxGeometry();
     const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
     const cube = new THREE.Mesh( geometry, material );
     scene.add( cube );
     /* CUBE end */

     let hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.61);
     hemiLight.position.set(0, 50, 0);
     // Add hemisphere light to scene
     scene.add(hemiLight);

     let d = 8.25;
     let dirLight = new THREE.DirectionalLight(0xffffff, 0.54);
     dirLight.position.set(-8, 12, 8);
     dirLight.castShadow = true;
     dirLight.shadow.mapSize = new THREE.Vector2(1024, 1024);
     dirLight.shadow.camera.near = 0.1;
     dirLight.shadow.camera.far = 1500;
     dirLight.shadow.camera.left = d * -1;
     dirLight.shadow.camera.right = d;
     dirLight.shadow.camera.top = d;
     dirLight.shadow.camera.bottom = d * -1;
     // Add directional Light to scene
     scene.add(dirLight);     



     /* camera.position.z = 5; */

     const loader = new THREE.GLTFLoader();
     let model = null;
     const to_remove = [];
     loader.load('res/rigged_figures/scene.gltf',
                 function (gltf) {
                   scene.add(gltf.scene);

                   console.log(gltf);

                   gltf.scene.position.y = -11;
                   gltf.scene.scale.set(7,7,7);

                   gltf.scene.traverse(o => {
                     if(o.isMesh) {
                       if(model === null) {
                         model = o;
                       } else {
                         o.visible = false;
                       }
                     }
                   });
                 }
     );

     function resizeRendererToDisplaySize(renderer) {
       const canvas = renderer.domElement;
       let width = window.innerWidth;
       let height = window.innerHeight;
       let canvasPixelWidth = canvas.width / window.devicePixelRatio;
       let canvasPixelHeight = canvas.height / window.devicePixelRatio;

       const needResize =
         canvasPixelWidth !== width || canvasPixelHeight !== height;
       if (needResize) {
         renderer.setSize(width, height, false);
       }
       return needResize;
     }

     function animate() {
       cube.rotation.x += 0.01;
       cube.rotation.y += 0.01;       

       if (resizeRendererToDisplaySize(renderer)) {
         const canvas = renderer.domElement;
         camera.aspect = canvas.clientWidth / canvas.clientHeight;
         camera.updateProjectionMatrix();
       }

       requestAnimationFrame( animate );
       renderer.render( scene, camera );

     }
     animate();
};

