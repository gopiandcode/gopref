open Containers

let js_bones = [
  "Root"; "Pelvis"; "Stolmach"; "Chest"; "Neck"; "Head"; "Chest.001";
  "ShoulderRotator.L"; "Shoulder.L"; "UpperArm.L"; "Elbow.L"; "Arm.L";
  "Wrist.L"; "Hand.L"; "ShoulderRotator.R"; "Shoulder.R"; "UpperArm.R"; "Elbow.R"; "Arm.R";
  "Wrist.R"; "Hand.R"; "Pelvis.002"; "UnderPelvis.L"; "UpperLeg.L"; "Knee.L"; "LowerLeg.L";
  "Ankle.L"; "Foot.L"; "Pelvis.003"; "UnderPelvis.R"; "UpperLeg.R"; "Knee.R"; "LowerLeg.R";
  "Ankle.R"; "Foot.R"; "ControllerPelvis"; "ControllerChest"; "ControllerHead"; "PoleKnee.L";
  "PoleKnee.R"; "PoleElbow.L"; "PoleElbow.R"; "ControllerFoot.L"; "ControllerAnkle.L"; "ControllerFoot.R";
  "ControllerAnkle.R"; "ControllerWrist.L"; "ControllerWrist.R"
] |> Iter.of_list |> Iter.zip_i |> Iter.map Pair.swap |> Hashtbl.of_iter

let find_bone_index name = Hashtbl.find js_bones name



let bindings = [
  "Pelvis.003" , [ "ControllerPelvis"];
  "Pelvis.002" , [ "ControllerPelvis"; ]; 
  "Pelvis" , [ "ControllerPelvis"]; 

  "Chest" , [ "ControllerChest" ]; 

  "ControllerHead" , [ "Head" ]; 

  "Neck" , [ "ControllerHead" ]; 
  "Head" , [ "ControllerHead" ]; 

  "Ankle.L" , [ "ControllerAnkle.L" ]; 
  "Ankle.R" , [ "ControllerAnkle.R" ]; 

  "Foot.L" , [ "ControllerFoot.L" ]; 
  "Foot.R" , [ "ControllerFoot.R" ]; 

  "Wrist.L" , [ "ControllerWrist.L" ]; 
  "Hand.L" , [ "ControllerWrist.L" ]; 

  "Wrist.R" , [ "ControllerWrist.R" ]; 
  "Hand.R" , [ "ControllerWrist.R" ]; 
]

let bindings2 = [
  "ControllerChest", ["Chest"; "Stolmach"; "Pelvis"; "Root"];
  "ControllerAnkle.L", ["Ankle.L"; "LowerLeg.L"; "Knee.L"; "UpperLeg.L"; "UnderPelvis.L"; "Pelvis.002"];
  "ControllerAnkle.R", ["Ankle.R"; "LowerLeg.R"; "Knee.R"; "UpperLeg.R"; "UnderPelvis.R"; "Pelvis.003"];
  "ControllerWrist.L", ["Wrist.L"; "Arm.L"; "Elbow.L"; "UpperArm.L"; "Shoulder.L"; "ShoulderRotator.L"];
  "ControllerWrist.R", ["Wrist.R"; "Arm.R"; "Elbow.R"; "UpperArm.R"; "Shoulder.R"; "ShoulderRotator.R"];
]

let mapping = [
  ("Root", [ "Pelvis"; "Pelvis.002"; "Pelvis.003"; "ControllerPelvis"; "ControllerChest" ]);
  ( "Pelvis", [ "Stolmach" ] );
  ( "Stolmach", [ "Chest" ] );
  ( "Chest", [ "Neck"; "Chest.001" ] );
  ( "Neck", [ "Head" ] );
  ( "Chest.001", [ "ShoulderRotator.L"; "ShoulderRotator.R" ] );
  ( "ShoulderRotator.L", [ "Shoulder.L" ] );
  ( "Shoulder.L", [ "UpperArm.L" ] );
  ( "UpperArm.L", [ "Elbow.L" ] );
  ( "Elbow.L", [ "Arm.L" ] );
  ( "Arm.L", [ "Wrist.L" ] );
  ( "Wrist.L", [ "Hand.L" ] );
  ( "ShoulderRotator.R", [ "Shoulder.R" ] );
  ( "Shoulder.R", [ "UpperArm.R" ] );
  ( "UpperArm.R", [ "Elbow.R" ]  );
  ( "Elbow.R", [ "Arm.R" ] );
  ( "Arm.R", [ "Wrist.R" ] );
  ( "Wrist.R", [ "Hand.R" ] );
  ( "Pelvis.002", [ "UnderPelvis.L" ] );
  ( "UnderPelvis.L", [ "UpperLeg.L" ] );
  ( "UpperLeg.L", [ "Knee.L" ] );
  ( "Knee.L", [ "LowerLeg.L" ] );
  ( "LowerLeg.L", [ "Ankle.L" ] );
  ( "Ankle.L", [ "Foot.L" ]  );
  ( "Pelvis.003", [ "UnderPelvis.R" ] );
  ( "UnderPelvis.R", [ "UpperLeg.R" ] );
  ( "UpperLeg.R", [ "Knee.R" ] );
  ( "Knee.R", [ "LowerLeg.R" ] );
  ( "LowerLeg.R", [ "Ankle.R" ] );
  ( "Ankle.R", [ "Foot.R" ] );
  ( "Head", [ ] );
  ( "Hand.L", [ ] );
  ( "Hand.R", [ ] );
  ( "Foot.L", [ ] );
  ( "Foot.R", [ ] );
  ( "ControllerPelvis", [ ] );
  ( "ControllerChest", [ ] );
  ( "ControllerHead", [ ]);
  ( "PoleKnee.L", [ ] );
  ( "PoleKnee.R", [ ] );
  ( "PoleElbow.L", [ ] );
  ( "PoleElbow.R", [ ] );
  ( "ControllerFoot.L", [ "ControllerAnkle.L" ] );
  ( "ControllerAnkle.L", [ ] );
  ( "ControllerFoot.R", [ "ControllerAnkle.R" ] );
  ( "ControllerAnkle.R", [ ] );
  ( "ControllerWrist.L", [ ] );
  ( "ControllerWrist.R", [ ] )
] |> Hashtbl.of_list

let rec find_paths (from: string) : string list list =
  match Hashtbl.find mapping from with
  | [] -> [[from]]
  | ls ->
    List.flat_map (fun (child: string) ->
      List.map (fun (v: string list) -> from :: v) (find_paths child : string list list)
    ) ls

let () =
  Jv.set Jv.global "paths" (Jv.of_list (Jv.of_list Jv.of_string) (find_paths "Root"))
  

let paths = [
  [ "Root"; "Pelvis"; "Stolmach"; "Chest"; "Neck"; "Head" ];
  [
    "Root"; "Pelvis"; "Stolmach"; "Chest"; "Chest.001";
    "ShoulderRotator.L"; "Shoulder.L"; "UpperArm.L"; "Elbow.L"; "Arm.L"; "Wrist.L"; "Hand.L"
  ];
  [
    "Root"; "Pelvis"; "Stolmach"; "Chest"; "Chest.001";
    "ShoulderRotator.R"; "Shoulder.R"; "UpperArm.R"; "Elbow.R"; "Arm.R"; "Wrist.R"; "Hand.R"
  ];
  [
    "Root"; "Pelvis.002";
    "UnderPelvis.L"; "UpperLeg.L"; "Knee.L"; "LowerLeg.L"; "Ankle.L"; "Foot.L"
  ];
  [
    "Root"; "Pelvis.003";
    "UnderPelvis.R"; "UpperLeg.R"; "Knee.R"; "LowerLeg.R"; "Ankle.R"; "Foot.R"
  ];
  [
    "Root"; "ControllerPelvis"
  ];
  [ "Root"; "ControllerChest" ]
]
