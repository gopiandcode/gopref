module H = struct
  include Tyxml_html
  let figure ?a elts = figure ?a elts
  let table ?a elts = table ?a elts
end


module B = Bulma.Bulma (H)
module UI = UI.Make (H)

let head =
  let stylesheet href = H.link ~rel:[`Stylesheet] ~href () in
  let charset charset = H.meta ~a:[H.a_charset charset] () in
  let viewport params = H.meta ~a:[H.a_name "viewport"; H.a_content params] () in
  H.head
    (H.title (H.txt "GopRef - Your one-stop figure reference buddy!"))
    [ stylesheet "style/style.css";
      H.link ~rel:[`Icon]  ~a:[H.a_mime_type "image/jpg"] ~href:"./res/gop-ref-logo.svg" ();
      charset "UTF-8";
      viewport "width=device-width, initial-scale=1"
    ]

let txt = H.txt
let a_class' v = H.a_class [v]
let a_href = H.a_href
let js_script src = H.script ~a:[H.a_mime_type "text/javascript"; H.a_src src] (H.txt "")
let inline_script src = H.script ~a:[H.a_mime_type "text/javascript"] (H.cdata_script src)
let noscript text = H.noscript [txt text]

let man_url = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimg1.cgtrader.com%2Fitems%2F25781%2F44e3e45684%2Fnasa-astronaut-apollo-3d-model-rigged-max-obj-3ds-fbx-c4d-lwo-lw-lws.jpg&f=1&nofb=1"

let _body =
  let open H in
  body [
    UI.navbar ();
    UI.notification_box [
      B.notification ~delete:true ~a_class:["is-danger"] [ txt "You have done something wrong" ]
    ];
    UI.main_split
      ~left:[ UI.renderer_canvas () ]
      ~right:[
        UI.half_pane [
          B.panel ~a_class:["is-primary"; "fill-height"] [
            UI.image_filter_input ();
            UI.results_list ([ UI.search_results_header (); ] @ List.init 10 (fun _ ->
              UI.search_result
                ~img:man_url ~name:"An epic astronaut!"
                ~tags:[
                  span ~a:[a_class' "tag"] [txt "everyday"];
                  span ~a:[a_class' "tag"] [txt "stylized"];
                  span ~a:[a_class' "tag"] [txt "anime"];
                ]
                ~similarity:"93%"
            ))
          ];
        ];
        B.divider "Image info";
        UI.quarter_pane [
          UI.image_info_box ~media_block:[
            UI.cropped_image ~src:man_url ~alt:"An epic astronaut!";
            B.block ~a_class:["has-text-centered"] [ UI.success_button "View" ];
            B.block ~a_class:["has-text-centered"] [ UI.danger_button "Delete" ];
          ]
            ~content:[
              UI.image_info_table
                ~title:[
                  B.control [ UI.editable_input "An epic astronaut!" ]
                ]
                ~source:[
                  UI.field_with_addon
                    ~field:(B.control ~a_class:["fill-width"] [ UI.read_only_input man_url ])
                    ~addon:(B.control [ UI.link_button "Embed" ])
                ]
                ~tags:[ UI.image_info_tag_list () ]
                ~pose:[ UI.danger_button "Update"; UI.link_button "Load"; ]
            ];

        ];
        B.divider "Upload Image";
        UI.quarter_pane [ UI.file_upload_panel () ]
      ];
    (* script "text/javascript" "js/three.js";
     * script "text/javascript" "js/three-ik.js";
     * script "text/javascript" "js/TextGeometry.js";
     * script "text/javascript" "js/CSS2DRenderer.js";
     * script "text/javascript" "js/FontLoader.js";
     * script "text/javascript" "js/GLTFLoader.js";
     * script "text/javascript" "js/TransformControls.js";
     * script "text/javascript" "js/CCDIKSolver.js";
     * script "text/javascript" "js/OrbitControls.js";
     * script "text/javascript" "js/jquery-3.6.0.js";
     * script "text/javascript" "libre-ref.js"; *)

    js_script "js/split.min.js";
    js_script "js/bulma-tagsinput.min.js";
    inline_script {| new BulmaTagsInput(document.getElementById("image-tag-filter")); |};
    inline_script {| new BulmaTagsInput(document.getElementById("image-info-tag-list")); |};
    inline_script {| Split(['#split-0', '#split-1']); |};
    inline_script {|
document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.getAttribute('aria-controls');
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }
});
|};
    noscript [
      a ~a:[a_href "licenses.html"] [txt "(Libre)"];
      txt "Javascript required"
    ]
  ]

let body =
  H.body [
    js_script "js/split.min.js";
    js_script "libre-ref.js";
    noscript "Javascript is required (don't worry, it's all Libre my friend!)"
  ]


let document =
  H.html
    ~a:[H.a_xmlns `W3_org_1999_xhtml; a_class' "has-navbar-fixed-top"]
    head
    body

let () =
  Format.printf "%a" (Tyxml_html.pp ~indent:true ~advert:"Generated with OCaml" ()) document

