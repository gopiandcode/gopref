[@@@warning "-26-34"]
open Containers
open Brr
open Brr_note
open Brr_note_kit
open Note

open Lib
open Common

module Reactive = UI.Reactive
module H = Bulma.Html
module B = Bulma.Bulma(H)
module UI = UI.Make(H)

let split tags =
  let split = Jv.get Jv.global "Split" in
  ignore @@ Jv.apply split [| Jv.of_list (fun v -> Jstr.of_string v |> Jv.of_jstr) tags |]

let initial_data =
  let man_url = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimg1.cgtrader.com%2Fitems%2F25781%2F44e3e45684%2Fnasa-astronaut-apollo-3d-model-rigged-max-obj-3ds-fbx-c4d-lwo-lw-lws.jpg&f=1&nofb=1" in
  Model.[
    (Data.v ~id:0 ~source:(`External (Jstr.v man_url))
       ~name:(Jstr.v "an epic astronaut")
       ~tags:(StringSet.of_list Jstr.[v "epic"; v "space"])
       ~pose:0.0);
    (Data.v ~id:1 ~source:(`External (Jstr.v man_url))
       ~name:(Jstr.v "a sad astronaut")
       ~tags:(StringSet.of_list Jstr.[v "anime"; v "cooll"])
       ~pose:0.0);
    (Data.v ~id:2 ~source:(`External (Jstr.v man_url))
       ~name:(Jstr.v "a mad astronaut")
       ~tags:(StringSet.of_list Jstr.[v "epic"; v "badass"])
       ~pose:0.0);
    (Data.v ~id:3 ~source:(`External (Jstr.v man_url))
       ~name:(Jstr.v "a blad astronaut")
       ~tags:(StringSet.of_list Jstr.[v "epic"; v "badass"])
       ~pose:0.0);
    (Data.v ~id:4 ~source:(`External (Jstr.v man_url))
       ~name:(Jstr.v "a glad astronaut")
       ~tags:(StringSet.of_list Jstr.[v "epic"; v "idiot"])
       ~pose:0.0);
    (Data.v ~id:5 ~source:(`External (Jstr.v man_url))
       ~name:(Jstr.v "a johnny astronaut")
       ~tags:(StringSet.of_list Jstr.[v "epic"; v "boi"])
       ~pose:0.0);
    (Data.v ~id:6 ~source:(`Embedded (Jstr.v man_url))
       ~name:(Jstr.v "a gaddy astronaut")
       ~tags:(StringSet.of_list Jstr.[v "epic"; v "soi"])
       ~pose:0.0);
  ]

let main () =
  let pose = S.const 1.0 in
  let body = Document.body G.document in
  let clipboard_ev = Reactive.capture_images_from_paste ~pose body in
  let handle_data_ev data ev =
    match ev with
    | `EmbedImage ->
      Console.(log [str "TODO: Embed image"]);
      data
    | `View ->
      Console.(log [str "TODO: View image"]);
      data
    |`LoadPose ->
      Console.(log [str "TODO: Load pose"]);
      data
    | `Delete -> Model.Scene.delete_selected data
    | `UpdatePose pose ->
      Model.(Scene.update_selected
               Fun.(Option.some % Data.set_pose pose)) data
    | `UpdateTags tags ->
      Model.(Scene.update_selected
               Fun.(Option.some % Data.set_tags tags)) data
    | `NewElement (source, name, tags, pose) ->
      Model.Scene.add_image ~source ~name ~tags ~pose data
    | `UpdateName name ->
      Model.(Scene.update_selected
               Fun.(Option.some % Data.set_name name)) data
    | `CloseViewer -> data in

  let handle_viewed_ev = fun (data, selected_image) -> function
    | `CloseViewer -> None
    | `View ->
      Model.Scene.selected data
      |> Option.map (fun data -> Model.Data.(image data, name data))
    | _ -> selected_image in

  let right_split = H.div ~a:[H.a_id "split-1"] [] in
  let main_element =
    H.div ~a:[H.a_class ["main-split";"split"]] [
      H.div ~a:[H.a_id "split-0"] [UI.renderer_canvas ()];
      right_split ] in

  let drop_ev =
    E.select [
      Reactive.capture_images_from_drop ~pose body;
      Reactive.capture_images_from_drop ~pose right_split;
      Reactive.capture_images_from_drop ~pose main_element;
    ] in

  let search_result_signal, image_viewer =
    S.fix (Model.Scene.init initial_data, None) (fun data ->
      let viewed_image = S.Pair.snd ~eq:Equal.(option (pair Jstr.equal Jstr.equal)) data in
      let data = S.Pair.fst ~eq:Model.Scene.equal data in
      (* ************************************** *)
      let search_results =
        S.map ~eq:(List.equal (Pair.equal Model.Data.equal Float.equal))
          Fun.(List.map (fun v -> (v, 99.0)) % Model.Scene.data)
          data in
      (* ************************************** *)
      let image_viewer, viewed_image_ev = Reactive.image_viewer viewed_image in

      let search_result_pane, data =
        let search_result_pane, selected_element =
          Reactive.search_results search_results in
        search_result_pane,
        (E.map Option.some selected_element)
        (* convert event to signal *)
        |> S.hold ~eq:(Option.equal Model.Data.equal) None
        (* set the selected element of data when clicked *)
        |> S.l2 ~eq:(Model.Scene.equal) (fun data selected ->
          selected
          |> Option.map (Fun.flip Model.Scene.set_selected data)
          |> Option.value ~default:data
        ) data in

      let selected_element =
        S.map ~eq:(Option.equal Model.Data.id_equal)
          Model.Scene.selected data in

      let main_panel_children, user_input_ev =
        let upload_image_pane, file_ev = Reactive.upload_image_box ~pose () in
        let signal =
          S.map ~eq:Equal.(pair (list @@ map El.to_jv Jv.equal) (list physical)) (function
            | None -> [search_result_pane] @ upload_image_pane, []
            | Some data -> 
              let image_info_pane, ev = Reactive.image_info ~pose data in
              [search_result_pane] @ image_info_pane @ upload_image_pane, [ev]
          ) selected_element  in
        let children = S.Pair.fst ~eq:Equal.(list (map El.to_jv Jv.equal)) signal in
        let evs =
          S.Pair.snd ~eq:Equal.physical signal
          |> S.map ~eq:Equal.physical (List.append [file_ev; clipboard_ev; drop_ev; viewed_image_ev])
          |> S.map ~eq:Equal.physical E.select
          |> E.swap in
        children, evs in

      let data = S.Pair.v data viewed_image in
      let data =
        S.sample data ~on:user_input_ev (fun ((data, _) as signal) ev ->
          handle_data_ev data ev, handle_viewed_ev signal ev
        )
        |> E.map (fun v -> S.const v)
        |> S.swap data in

      Elr.def_children right_split main_panel_children;
      data, (data, image_viewer)
    ) in

  Logr.hold (S.log search_result_signal ignore);
  El.append_children body [
    UI.navbar ();
    UI.notification_box [];
    main_element;
    image_viewer
  ];
  split ["#split-0"; "#split-1"]

let () =
  try
    main ()
  with
  | exn -> Console.(log [str @@ Printexc.to_string exn])
  
