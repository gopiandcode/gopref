[@@@warning "-26"]
open Containers
open Brr
open Brr_note
open Utils

let console_log args = ignore @@ Jv.call (Jv.get Jv.global "console") "log" args
let document = Jv.get Jv.global "document"
let window = Jv.get Jv.global "window"

let get_element_by_id elt = Document.find_el_by_id G.document (Jstr.v elt)

let log_error =
  let error_box = Document.find_el_by_id G.document (Jstr.v "error-box") |> Option.get_exn_or "not found" in
  let error_click_handler = fun evt ->
    let target = Ev.target_to_jv (Ev.current_target evt) in
    let parent = El.parent target |> Option.get_exn_or "not found" in
    El.remove parent;
    Ev.stop_propagation evt in
  fun err ->
    let button =
      El.button
        (* ~d:G.document *)
        ~at:[At.class' (Jstr.v "btn");
             At.class' (Jstr.v "btn-clear");
            At.class' (Jstr.v "float-right")]
        [ ] in
    let toast =
      El.div ~at:[At.class' (Jstr.v "toast");
                  At.class' (Jstr.v "toast-error")] [ button; El.txt' err ] in
    Ev.listen Ev.click error_click_handler (El.as_target button);
    El.append_children error_box [toast];
    ()

let log_info =
  let error_box = Document.find_el_by_id G.document (Jstr.v "error-box") |> Option.get_exn_or "not found" in
  let error_click_handler = fun evt ->
    let target = Ev.target_to_jv (Ev.current_target evt) in
    let parent = El.parent target |> Option.get_exn_or "not found" in
    El.remove parent;
    Ev.stop_propagation evt in
  fun err ->
    let button =
      El.button
        (* ~d:G.document *)
        ~at:[At.class' (Jstr.v "btn");
             At.class' (Jstr.v "btn-clear");
            At.class' (Jstr.v "float-right")]
        [ ] in
    let toast =
      El.div ~at:[At.class' (Jstr.v "toast");
                  At.class' (Jstr.v "toast-warning")] [ button; El.txt' err ] in
    Ev.listen Ev.click error_click_handler (El.as_target button);
    El.append_children error_box [toast];
    ignore @@ G.set_timeout ~ms:2000  (fun () ->
      El.remove toast;
    );
    ()


let image_list image_item =
  let children : El.t list Note.signal =
    fst @@ Note.S.create []
  in
  Elr.def_children image_item children

let build_ik_system : Three.IK.t -> (string, Three.Bone.t) Hashtbl.t -> unit =
  fun ik bone_mapping ->
  let open Three in
  let angle = 360.0 in
  let (!!) v = Hashtbl.find bone_mapping v in
  let (+=) = IK.Chain.add in
  let add = IK.Chain.add in
  let connect = IK.Chain.connect in
  let bone v = Bone.upcast (!! v) in
  let joint ?angle bone  =
    IK.Joint.create ~bone
      ~constraints:(match angle with
          None -> [| |]
        | Some angle -> [| IK.Constraint.ball angle |]) in
  let joint_mapping = Hashtbl.create 10 in
  let get ?angle name =
    let res = ref None in
    Hashtbl.update joint_mapping
      ~k:name
      ~f:(fun _ -> function
        | None ->
          let joint = joint ?angle (!! name) in
          res := Some joint;
          Some joint
        | Some joint -> res := Some joint; Some joint
      );
    Option.get_exn_or "logic error" !res in
  let (!@) j = Hashtbl.find joint_mapping j in

  let chain f =
    let new_chain = IK.Chain.create () in
    IK.add ik (f new_chain) in
  let connect chain f =
    let new_chain = IK.Chain.create () in
    connect chain (f new_chain) in
  chain (fun chain ->
    chain += get "Pelvis";
    chain += get ~angle:90.0 "Stolmach";
    add ~target:(bone "ControllerChest") chain (get "Chest");
    chain
  );

  chain (fun chain ->
    (* chain += get  "Pelvis002"; *)
    chain += get  "UnderPelvisL";
    chain += get  "UpperLegL";
    chain += get  "KneeL";
    chain += get  "LowerLegL";
    add ~target:(bone "ControllerAnkleL") chain (get ~angle:90. "AnkleL");
    chain
  );

  chain (fun chain ->
    (* chain += get  "Pelvis003"; *)
    chain += get  "UnderPelvisR";
    chain += get  "UpperLegR";
    chain += get  "KneeR";
    chain += get  "LowerLegR";
    add ~target:(bone "ControllerAnkleR") chain (get ~angle:90. "AnkleR");
    chain
  );


  chain (fun chain ->
    chain += get  "ShoulderRotatorL";
    chain += get  "ShoulderL";
    chain += get  "UpperArmL";
    chain += get  "ElbowL";
    chain += get  "ArmL";
    add ~target:(bone "ControllerWristL") chain (get ~angle:90. "WristL");
    chain
  );

  chain (fun chain ->
    chain += get  "ShoulderRotatorR";
    chain += get  "ShoulderR";
    chain += get  "UpperArmR";
    chain += get  "ElbowR";
    chain += get  "ArmR";
    add ~target:(bone "ControllerWristR") chain (get ~angle:90. "WristR");
    chain
  );

  chain (fun chain ->
    (* chain += get  "Pelvis002"; *)
    chain += get  "UnderPelvisL";
    chain += get  "UpperLegL";
    add ~target:(bone "PoleKneeL") chain (get ~angle:90. "KneeL");
    chain
  );

  chain (fun chain ->
    (* chain += get  "Pelvis003"; *)
    chain += get  "UnderPelvisR";
    chain += get  "UpperLegR";
    add ~target:(bone "PoleKneeR") chain (get ~angle:90. "KneeR");
    chain
  );

  chain (fun chain ->
    chain += get  "ShoulderRotatorR";
    chain += get  "ShoulderR";
    chain += get  "UpperArmR";
    add ~target:(bone "PoleElbowR") chain (get ~angle:90. "ElbowR");
    chain
  );
  chain (fun chain ->
    chain += get  "ShoulderRotatorL";
    chain += get  "ShoulderL";
    chain += get  "UpperArmL";
    add ~target:(bone "PoleElbowL") chain (get ~angle:90. "ElbowL");
    chain
  );



  ()
  

let example () =
  console_log [|Jv.of_string "starting example"|];
  let background_color = Three.Color.create_hex 0xf1f1f1 in
  let canvas = Document.find_el_by_id G.document (Jstr.v "render-canvas")
               |> Option.get_exn_or "canvas not found" in

  let renderer = Three.WebGLRenderer.create ~alpha:true ~canvas () in
  Three.WebGLRenderer.set_pixel_ratio renderer
    (Jv.to_float @@ Jv.get window "devicePixelRatio");

  let camera =
    let fov = 75. in
    let aspect =
      El.prop (El.Prop.float @@ Jstr.v "innerWidth")  (El.of_jv Jv.global) /.
      El.prop (El.Prop.float @@ Jstr.v "innerHeight")  (El.of_jv Jv.global) in
    let near = 0.1 and far = 10000. in
    Three.PerspectiveCamera.create
      ~fov ~aspect ~near ~far in
  Three.Vector3.set
    (Three.PerspectiveCamera.position camera) ~x:0. ~y:(-.3.) ~z:30.;

  let scene = Three.Scene.create () in
  Three.Scene.set_background scene background_color;
  Three.Scene.set_fog scene (Three.Fog.create ~color:0xf1f1f1 ~near:80. ~far:1000.);


  let ik = Three.IK.create () in
  console_log [|Jv.of_string "built ik"|];
  Jv.set Jv.global "ik" (Three.IK.to_jv ik);

  Jv.set Jv.global "camera" (Three.PerspectiveCamera.to_jv camera);

  let controls = Three.OrbitControls.create ~camera ~input_source:canvas in

  let ik_setup = ref false in

  Three.(controls.@[OrbitControls.enable_pan] <- false);
  Three.(controls.@[OrbitControls.target].@[Vector3.y] <- 10.);
  let rec animate _ =
    ignore @@ G.request_animation_frame animate;
    Three.OrbitControls.update controls;
    if !ik_setup then Three.IK.solve ik;
    Three.WebGLRenderer.render renderer ~scene ~camera;
  in
  let on_resize _ _ =
    console_log [| Jv.of_string "resizing" |];
    let canvas = El.to_jv canvas in
    let width = Jv.to_float @@ Jv.get canvas "clientWidth"
    and height = Jv.to_float @@ Jv.get canvas "clientHeight" in
    Three.WebGLRenderer.set_size renderer ~width ~height ~update_style:true;
    Three.PerspectiveCamera.set_aspect camera (width /. height);
    Three.PerspectiveCamera.update_projection_matrix camera;
  in

  let scene_obj = ref None in
  let mesh_array = ref [] in
  let loader = Three.GLTFLoader.create () in
  Three.GLTFLoader.load ~on_error:(fun err -> log_error (Jv.to_string err))
    loader ~path:"./res/puppet-modified.gltf" ~on_load:(fun v ->
      let text_mesh_material = Three.Material.mesh_basic ~color:(Three.Color.create_hex 0xf1f10f) () in
      let bone_mesh_material = Three.Material.mesh_basic ~color:(Three.Color.create_hex 0xf10543) () in
      (* Three.(bone_mesh_material.@[Material.side] <- `Double); *)
      Three.(bone_mesh_material.@[Material.wireframe] <- true);


      let count = ref 0 in

      Three.(GLTF.scene v |> Object3D.traverse) (fun el ->
        begin match Three.Object3D.ty el with
        | `SkinnedMesh -> incr count; Jv.set Jv.global "skinned_mesh" (Three.Object3D.to_jv el)
        | `Bone -> Jv.set Jv.global "bone" (Three.Object3D.to_jv el)
        | _ -> ()
        end;

        begin let open Three in
          match Object3D.downcast el with
          | `SkinnedMesh m ->
            let (let+) x f = if x then f () else () in
            (* m.@[SkinnedMesh.material].@[Material.wireframe] <- true; *)
            SkinnedMesh.pose m;


            let helper =
              SkeletonHelper.create (Object3D.parent (m :> Object3D.t) |> Option.get_exn_or "") in

            Scene.add scene (SkeletonHelper.to_jv helper);

            Jv.set Jv.global "skellie" (SkeletonHelper.to_jv helper);

            let skeleton = SkinnedMesh.skeleton m in
            let bones = Skeleton.bones skeleton in
            let+ () = Array.length bones > 0 in

            let bone_mapping =
              Array.to_iter bones
              |> Iter.map (fun v -> ((Bone.upcast v).@[Object3D.name], v))
              |> Hashtbl.of_iter in
            build_ik_system ik bone_mapping;

            
            let root_bone = bones.(0) in
            Axis_utils.set_z_forward root_bone;
            SkinnedMesh.bind m skeleton;


            let helper = Three.IK.Helper.create ik in
            Three.Scene.add scene (Three.IK.Helper.to_jv helper);
            ik_setup := true;
            
            
            (* Jv.set Jv.global "helper" (IK.Helper.to_jv helper);
             * Jv.set Jv.global "bones" (Jv.of_array Bone.to_jv bones); *)

          | `Bone root_bone ->
            let mesh =
              Mesh.create
                ~geometry:(Geometry.sphere ~radius:0.125 ())
                ~material:bone_mesh_material in
            (mesh :> Object3D.t).@[Object3D.name] <- (el.@[Object3D.id]);
            Object3D.add (Bone.upcast root_bone) (mesh :> Object3D.t);

            let text =
              El.div ~at:[At.class' (Jstr.of_string "label")] [
                El.txt' ((Bone.upcast root_bone).@[Object3D.name])
              ] in
            let obj = Three.CSS2DObject.create text in

            Object3D.add (Bone.upcast root_bone) (obj :> Object3D.t);

          | _ -> ()
        end;

        (* console_log [| Jv.of_string @@ Three.(el.@[Object3D.name]) |] *)
      );
      console_log [| Jv.of_string "no of meshes:"; Jv.of_int !count |];

      Three.(GLTF.scene v |> Object3D.traverse) (fun el ->
        begin match Three.Object3D.ty el with
        | `Mesh -> mesh_array := el :: !mesh_array
        | _ -> ()
        end
      );
      console_log [| Jv.of_string "no of bone meshes:"; Jv.of_int (List.length !mesh_array) |];

      scene_obj := Some (Three.GLTF.scene v);

      Three.Scene.add scene Three.(Object3D.to_jv (GLTF.scene v));

      Jv.set Jv.global "model" (Three.GLTF.to_jv v);

      console_log [| Jv.of_string "loaded";  Three.GLTF.to_jv v |]
    );

  let raycaster = Three.Raycaster.create () in
  Jv.set Jv.global "raycaster" (Three.Raycaster.to_jv raycaster);

  let transform_controls = Three.TransformControls.create ~camera ~element:canvas in

  Three.(Scene.add scene (TransformControls.to_jv transform_controls));

  let canvas_click_handler (mouse: Ev.Mouse.t Ev.t) =
    Ev.prevent_default mouse;
    console_log [|Jv.of_string "handling click?"|];
    let (x,y) =
      let client_x = Ev.Mouse.client_x (Ev.as_type mouse) in
      let client_y = Ev.Mouse.client_y (Ev.as_type mouse) in
      let rect_left, rect_right, rect_top, rect_bottom =
        let canvas = El.to_jv canvas in
        let client_rect = Jv.call canvas "getBoundingClientRect" [| |] in
        let (!!) v = Jv.to_float @@ Jv.get client_rect v in
        let left = !! "left" and right = !! "right"
        and top = !! "top" and bottom = !! "bottom" in
        left,right,top,bottom in
      (* mouse.x = ( ( event.clientX - rect.left ) / ( rect.right - rect.left ) ) * 2 - 1; *)
      let x = ((client_x -. rect_left) /. (rect_right -. rect_left)) *. 2. -. 1. in
      (* mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1; *)
      let y = (-. ((client_y -. rect_top) /. (rect_bottom -. rect_top)) *. 2.) +. 1. in
      x,y in

    (* console_log [| Jv.of_string "got click at "; Jv.of_float x; Jv.of_float y; |]; *)
    Three.PerspectiveCamera.update_matrix_world camera;
    Three.Raycaster.set_from_camera raycaster ~coords:(Three.Vector2.create ~x ~y ()) ~camera;

    (* let () =
     *   let open Three in
     *   let material =
     *     Material.line_basic ~color:(Color.create_hex 0x0000ff) () in
     *   let geometry = 
     *     Geometry.buffer ~points:[|
     *       raycaster.@[Raycaster.ray].@[Ray.origin];
     *       raycaster.@[Raycaster.ray].@[Ray.direction];
     *     |] () in
     *   console_log [| Jv.of_string "creating line" |];
     *   let line = Line.create ~geometry ~material in
     *   console_log [| Jv.of_string "created line" |];
     *   Scene.add scene (Line.to_jv line);
     *   () in
     * console_log [| Jv.of_string "added line" |]; *)

    let intersects = Three.Raycaster.intersect_objects raycaster (* Three.(scene.@[Scene.children]) *) (Array.of_list !mesh_array) in
    console_log [| Jv.of_string "intersected objects (pre-filter):";
                   (Jv.of_jv_array (Array.map Three.Intersection.to_jv intersects)) |];

    let intersected_bone_meshes =
      Array.filter_map Three.(fun inter ->
        let inter = inter.@[Intersection.obj] in
        match Object3D.downcast inter with
        | `Mesh v -> Some v
        | _ -> None
      ) intersects in

    if Array.length intersected_bone_meshes > 0 then begin
      let open Three in
      let (let+) x f = match x with None -> () | Some v -> f v in
      let bone_mesh = intersected_bone_meshes.(0) in
      let+ root_bone = Object3D.parent (bone_mesh :> Object3D.t) in
      log_info (root_bone.@[Object3D.name]);
      console_log [| Jv.of_string "selected "; Jv.of_string root_bone.@[Object3D.name] |];
      begin
        let name = (Jv.to_jstr (Jv.get (Object3D.to_jv root_bone) "name")) in
        if Jstr.starts_with ~prefix:(Jstr.of_string "Controller") name ||
            Jstr.starts_with ~prefix:(Jstr.of_string "Pole") name
        then transform_controls.@[TransformControls.mode] <- `Translate
        else  transform_controls.@[TransformControls.mode] <- `Rotate
      end;

      TransformControls.attach transform_controls root_bone;
      controls.@[OrbitControls.enabled] <- false;
      ()
    end else begin
      let open Three in
      controls.@[OrbitControls.enabled] <- true;
      Three.TransformControls.detach transform_controls; 
    end;
    console_log [| Jv.of_string "intersected objects:";
                   (Jv.of_jv_array (Array.map Three.Mesh.to_jv intersected_bone_meshes)) |];

    ()
  in

  Ev.listen Ev.click canvas_click_handler (El.as_target canvas);

  let create_sphere (posn) =
    let geometry = Three.Geometry.sphere ~radius:0.25 () in
    let material = Three.Material.mesh_basic ~color:(Three.Color.create_hex 0x880012) () in
    let sphere = Three.Mesh.create ~geometry ~material in
    let position = Three.Object3D.((sphere :> t).@[position]) in
    let () =
      let (x,y,z) = Three.Vector3.get posn in
      Three.Vector3.set position ~x ~y ~z in
    Three.Scene.add scene (Three.Mesh.to_jv sphere);
    sphere in
  Jv.set Jv.global "create_sphere" (Jv.repr create_sphere);
  Jv.set Jv.global "scene" (Three.Scene.to_jv scene);

  let grid_helper = Three.PolarGridHelper.create () in
  Three.Scene.add scene (Three.PolarGridHelper.to_jv grid_helper);

  let observer = ResizeObserver.create on_resize in
  ResizeObserver.observe observer canvas  ~box:`ContentBox;
  ignore @@ G.request_animation_frame animate;

  ()

let f () =
  let image_body = get_element_by_id "image-list" |> Option.get_exn_or "image-list not found" in
  image_list image_body;
  let _ = try example () with exn -> log_error (Printexc.to_string exn) in
  let _ = console_log [| Jv.global |] in
  let _ =
    let (let+) x f = match x with None -> () | Some v -> f v in
    let+ canvas = get_element_by_id "render-canvas" in
    ignore @@ console_log [| El.to_jv canvas |] in

  

  print_endline "hello world!"
