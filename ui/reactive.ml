open Containers
open Brr
open Brr_note
open Brr_note_kit
open Note

module Model = Lib.Model
module H = Bulma.Html
module B = Bulma.Bulma(H)
module UI = Builder.Make(H)

module StringSet = Set.Make (Jstr)
let (@~>) el key = E.filter (Key.equal key) (Evr.on_el Ev.keydown Key.of_ev el)

let el_equal = Equal.map El.to_jv Jv.equal

let list_replace_first pred v =
  let rec loop acc = function
    | [] -> List.rev acc
    | h :: t when pred h -> List.rev_append (v :: acc) t
    | h :: t -> loop (h :: acc) t in
  loop []

let alt = At.v (Jstr.v "alt")
let image ?(at=[]) ~src ~alt:alt' () =
  El.p ~at:([At.class' (Jstr.v "image")] @ at) [
    El.img ~at:[
      At.src src;
      alt alt' ] ()
  ]

let cropped_image ?(at=[]) ~src ~alt:alt' () =
  B.block [
    image ~at:([
      At.class' (Jstr.v "is-128x128");
      At.class' (Jstr.v "cropped-image");
      At.class' (Jstr.v "has-text-centered");
    ] @ at) ~src ~alt:alt' ()
  ]

let button ?ty txt ev =
  let button = match ty with
    | None -> B.button txt
    | Some `Danger -> UI.danger_button txt
    | Some `Link -> UI.link_button txt
    | Some `Success -> UI.success_button txt in
  let event = Evr.on_el Ev.click (fun _ -> ev) button in
  button, event

let read_only_input value =
  B.input ~a:[H.a_input_type `Text; [At.value value]; H.a_readonly ()] ()




let tag_input ?(init=StringSet.empty) placeholder =
  let element, input =
    let input =   B.input ~a:[ H.a_input_type `Text; H.a_placeholder placeholder ] () in
    let tags_input = H.div ~a:[H.a_class ["tags-input"]] [ input ] in
    tags_input, input in
  let add_tag_event =
    let return = input @~> `Return in
    let tag_text = E.map (fun _ -> input |>  El.prop El.Prop.value |> Jstr.trim) return in
    let evt = E.filter_map (function v when Jstr.is_empty v -> None | v -> Some (`AddTag v)) tag_text in
    Elr.set_prop El.Prop.value input ~on:(E.stamp evt Jstr.empty);
    Elr.set_has_focus input ~on:(E.stamp evt true);
    evt in
  let create_tag txt = 
    let remove_tag_event (el, txt) = Evr.on_el Ev.click (fun _ -> `RemTag txt) el in
    let tag, delete = B.tag_delete (El.txt txt) in
    tag, remove_tag_event (delete, txt) in
  let do_action action set = match action with
    | `AddTag v -> StringSet.add v set
    | `RemTag v -> StringSet.remove v set in
  let build_tag_list set =
    let build_tags set = StringSet.to_iter set |> Iter.map create_tag |> Iter.to_list |> List.split in
    let items =
      S.map ~eq:(Pair.equal (List.equal el_equal) @@ List.equal Equal.physical) build_tags set in
    let action =
      E.swap @@
      S.map ~eq:(Equal.physical) (fun (_, evts) -> E.select (add_tag_event :: evts)) items in
    let children =
      S.map ~eq:(List.equal el_equal) (fun items -> fst items @ [input]) items in
    Elr.def_children element children;
    action in
  let update_tag_set signal =
    let action = build_tag_list signal in
    let action = (E.map do_action action) in
    let signal = S.accum ~eq:StringSet.equal (S.value signal) action in
    signal, signal in
  let signal = S.fix init update_tag_set in
  element, signal

let editable_input ?(init=Jstr.empty) () =
  let input = B.input ~a:[H.a_input_type `Text; [At.value init]] () in
  let txt_change = Evr.on_el Ev.input (fun _ -> El.prop El.Prop.value input) input in
  let signal = S.hold ~eq:Jstr.equal init txt_change in
  (* let signal =
   *   S.fix init (fun _signal ->
   *     signal, signal
   *   ) in *)
  input, signal


let search_results list =
  let element, table =
    let table =
      B.table
        ~a_class:["is-striped"; "is-fullwidth"; "is-hoverable"; "is-align-self-flex-start"] [] in
    B.panel_block ~a_class:["scroll-y"] [
      table
    ], table in
  let build_search_result (data, similarity) =
    let element =
      UI.search_result'
        ~img:(El.img ~at:Lib.Model.Data.[At.src (image data); alt (name data)] ())
        ~name:(El.txt (Model.Data.name data))
        ~tags:(StringSet.to_iter (Model.Data.tags data)
               |> Iter.map (fun v -> UI.tag (El.txt v)) |> Iter.to_list)
        ~similarity:(El.txt (Jstr.of_float similarity)) in
    let selected = Evr.on_el Ev.click (fun _ -> data) element in
    element, selected in
  let selected =
    S.map
      ~eq:(Equal.pair (Equal.list @@ Equal.map El.to_jv Jv.equal) Equal.physical)
      (fun ls -> List.map build_search_result ls |> List.split) list in
  let action = E.swap @@ S.map ~eq:(Equal.physical) (fun (_, evs) -> E.select evs) selected in
  let () =
    let header = UI.search_results_header () in
    let children = S.map ~eq:(List.equal @@ Equal.map El.to_jv Jv.equal)
                     (fun (children, _) -> header :: children) selected in
    Elr.def_children table children in
  element, action


let image_info ~pose data =
  let image_tag_input, tag_update_ev = tag_input ~init:(Model.Data.tags data) "Add tags" in
  let tag_update_ev = E.map (fun tags -> `UpdateTags tags) (S.changes tag_update_ev) in
  let editable_input, name_update_ev = editable_input ~init:(Model.Data.name data) () in
  let name_update_ev = E.map (fun name -> `UpdateName name) (S.changes name_update_ev) in
  let view_button, view_ev = button ~ty:`Success "View" `View in
  let delete_button, delete_ev = button ~ty:`Danger "Delete" `Delete in
  let update_pose_button, update_pose_ev = button ~ty:`Danger "Update" `Update in
  let update_pose_ev =
    let pose_vl = S.snapshot pose ~on:update_pose_ev in
    E.map (fun pose -> `UpdatePose pose) pose_vl in
  let load_pose_button, load_pose_ev = button ~ty:`Link "Load" `LoadPose in
  let source, embed_image_ev =
    match Model.Data.source data with
    | `External url ->
      let embed_image_button, embed_image_ev = button ~ty:`Link "Embed" `EmbedImage in
      UI.field_with_addon
        ~field:(B.control ~a_class:["fill-width"] [ read_only_input url ])
        ~addon:(B.control [ embed_image_button ]), Some embed_image_ev
    | `Embedded _ ->
      read_only_input (Jstr.v "static"), None in

  let ev =
    E.select (Option.to_list embed_image_ev @ [
      tag_update_ev; name_update_ev; view_ev; delete_ev; update_pose_ev; load_pose_ev
    ]) in
  [
    B.divider "Image info";
    UI.quarter_pane [
      UI.image_info_box ~media_block:[
        cropped_image ~src:(Model.Data.image data) ~alt:(Model.Data.name data) ();
        B.block ~a_class:["has-text-centered"] [ view_button ];
        B.block ~a_class:["has-text-centered"] [ delete_button ];
      ]
        ~content:[
          UI.image_info_table
            ~title:[
              B.control [ editable_input ]
            ]
            ~source:[source]
            ~tags:[ image_tag_input ]
            ~pose:[ B.container [B.columns [B.column [update_pose_button];
                                            B.column [load_pose_button]]] ]
        ];
    ]], ev


let search_results data =
  let tag_filter_element, tag_filter = tag_input "Filter tags" in
  let search_results, selected_element =
    let search_result_signal =
      S.l2 ~eq:(List.equal (Pair.equal Model.Data.equal Float.equal))
        Model.Data.tags_is_subset data tag_filter in
    search_results search_result_signal in
  UI.half_pane [
    B.panel ~a_class:["is-primary"; "fill-height"] [
      B.panel_block ~a_class:["image-filter-block"] [ tag_filter_element  ];
      search_results
    ] ], selected_element

let create_image_event_from_file_data pose data = match data with
  | (`File, data) -> `NewElement (`Embedded data, Jstr.empty, StringSet.empty, pose)
  | (`String, data) -> `NewElement (`External data, Jstr.empty, StringSet.empty, pose)

let file_upload_panel () =
  let input = El.input ~at:(At.class' (Jstr.v "file-input") :: H.a_input_type `File) () in
  let file_upload =
    El.div ~at:([At.class' (Jstr.v "file")]) [
      El.label ~at:(H.a_class ["file-label"]) [
        input;
        El.span ~at:(H.a_class ["file-cta"]) [
          El.span ~at:(H.a_class ["file-icon"]) [
            El.i ~at:(H.a_class ["fas"; "fa-upload"]) [El.txt (Jstr.v "📂")];
          ];
          El.span ~at:(H.a_class ["file-label"]) [ H.txt "Choose a file..." ]
        ]
      ]
    ] in
  let ev =
    Evr.on_el Ev.input Ev.as_type input
    |> E.map (fun _ -> Lib.Helpers.File_list.of_input input)
    |> Fun.flip E.bind (fun data ->
      Lib.Helpers.File_list.extract_files data
      |> Futr.to_event
    ) in
  let ev = E.filter_map (Fun.id) ev in
  B.level ~a_class:["upload-file-level"] [ file_upload ], ev

let image_viewer image =
  let image_container, image_elt = B.image' ~src:"" ~alt:"" () in
  let _eq = Equal.(option (pair Jstr.equal Jstr.equal)) in
  let elt, close_button = B.modal ~background:[] [ image_container ] in
  Elr.set_class (Jstr.v "is-active") ~on:(S.Option.is_some image |> S.changes) elt;
  Elr.def_at (Jstr.v "src") (S.Option.map ~eq:(Option.equal Jstr.equal) fst image) image_elt;
  Elr.def_at (Jstr.v "alt") (S.Option.map ~eq:(Option.equal Jstr.equal) snd image) image_elt;
  let image = (Evr.on_el Ev.click (fun _ -> `CloseViewer) close_button) in
  elt, image


let upload_image_box ~pose () =
  let upload_panel, ev = file_upload_panel () in
  let ev = S.sample pose ~on:ev create_image_event_from_file_data in
  [B.divider "Upload Image"; UI.quarter_pane [ upload_panel ]], ev

let capture_images_from_paste ~pose body =
  let paste_ev = Evr.on_el Ev.paste Fun.id body in
  paste_ev
  |> E.map Ev.as_type 
  |> E.filter_map Ev.Clipboard.data
  |> Fun.flip E.bind (fun data ->
    data
    |> Lib.Helpers.Data_transfer.extract_data_string
    |> Futr.to_event
    |> E.filter_map Fun.id
    |> (fun v on -> S.sample pose ~on v) create_image_event_from_file_data      
  ) 

let capture_images_from_drop ~pose body =
  let drop_ev = Evr.on_el ~default:true Ev.drop Fun.id body in
  drop_ev
  |> E.map Ev.as_type
  |> E.filter_map Ev.Drag.data_transfer
  |> Fun.flip E.bind (fun data ->
    data
    |> Lib.Helpers.Data_transfer.extract_data_string
    |> Futr.to_event
    |> E.filter_map Fun.id
    |> (fun v on -> S.sample pose ~on v) create_image_event_from_file_data      
  ) 
