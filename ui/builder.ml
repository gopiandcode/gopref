open Bulma.Types

let o_cons opt v = match opt with None -> [v] | Some ls -> v :: ls
let o_cat opt v = match opt with None -> v | Some ls -> v @ ls

module Make (H: HTML_SIG) = struct

  module B = Bulma.Bulma(H)

  let txt = H.txt
  let a_id = H.a_id
  let a_class = H.a_class
  let a_class' v = H.a_class [v]

  let navbar () =
    let open H in
    B.(navbar ~a_class:["is-fixed-top"; "is-light";"is-transparent"] [
      navbar_brand [
        navbar_item_a [
          img
            ~a:[a_width 112; a_height 28]
            ~src:"res/gop-ref-logo-title.png"
            ~alt:"GopRef: Your one-stop reference buddy!" ()
        ];
        navbar_burger ~a:[a_aria "controls" ["nav-menu"]] ()
      ];
      navbar_menu ~a:[a_id "nav-menu"] ([],[
        navbar_item [
          div ~a:[a_class ["field"; "is-grouped"]] [
            H.a ~a:[a_class ["bd-donate-button"; "button"]] [
              txt "Donate ❤"
            ]
          ]
        ];
        navbar_item_with_dropdown (navbar_item_a [txt "File"]) [
          navbar_item_a [txt "New Project"];
          navbar_item_a [txt "Save Project"];
          navbar_item_a [txt "Load Project"];
        ];
        navbar_item_a [txt "Settings"];
        navbar_item_a [txt "About"];
      ]);
    ])


  let notification_box elts =
    H.div ~a:[a_class' "notification-box"] elts

  let main_split ~left ~right =
    H.div ~a:[a_class ["main-split";"split"]] [
      H.div ~a:[a_id "split-0"] left;
      H.div ~a:[a_id "split-1"] right
    ]

  let renderer_canvas () = H.canvas ~a:[a_id "renderer-canvas"] []

  let half_pane elts =
    H.div ~a:[a_class ["half-pane"]] [
      H.div ~a:[a_class ["half-pane-content"]] elts
    ]

  let quarter_pane elts =
    H.div ~a:[a_class ["quarter-pane"]] [
      H.div ~a:[a_class ["half-pane-content"]] elts
    ]

  let file_upload_panel () = B.level ~a_class:["upload-file-level"] [ B.file_upload (); ]

  let image_filter_input () =
    B.panel_block ~a_class:["image-filter-block"] [
      B.input  ~a:[
        a_id "image-tag-filter";
        H.a_input_type `Text;
        H.a_placeholder "Filter tags" ] ()
    ]

  let image_info_tag_list () =
    B.input ~a:[ H.a_input_type `Text; H.a_placeholder "Add tags"] ()    

  let results_list elts =
    B.panel_block ~a_class:["scroll-y"] [
      B.table ~a_class:["is-striped"; "is-fullwidth"; "is-hoverable"; "is-align-self-flex-start"] elts
    ]

  let search_results_header () =
    H.tr [ H.th [H.txt "Image"]; H.th [H.txt "Details"]; H.th [H.txt "Similarity"]; ]

  let search_result ~img ~name ~tags ~similarity =
    H.tr [
      H.td [
        H.figure ~a:[a_class [
          "image"; "is-64x64"; "is-flex"; "cropped-image";
          "is-align-items-center"; "is-justify-center"
        ]] [H.img ~src:img ~alt:name ()]
      ];
      H.td [
        H.div ~a:[a_class' "content"] [
          H.p [txt name];
          H.div ~a:[a_class' "tags"] tags
        ]
      ];
      H.td [txt similarity]
    ]    

  let search_result' ~img ~name ~tags ~similarity =
    H.tr [
      H.td [
        H.figure ~a:[a_class [
          "image"; "is-64x64"; "is-flex"; "cropped-image";
          "is-align-items-center"; "is-justify-center"
        ]] [img]
      ];
      H.td [
        H.div ~a:[a_class' "content"] [
          H.p [name];
          H.div ~a:[a_class' "tags"] tags
        ]
      ];
      H.td [similarity]
    ]    


  let image_info_box ~media_block ~content =
    H.div ~a:[a_class ["box"; "image-info-box"]] [
      B.level [
        B.media ~a_class:["fill-width"]
          ~left:media_block
          [H.div ~a:[a_class' "content"] content]

      ];
    ]    

  let image_info_table ~title ~source ~tags ~pose =
    B.table ~a_class:["is-fullwidth"; "image-info-table"] [
      H.tr [ H.td [txt "Title"]; H.td  title  ];
      H.tr [ H.td [txt "Source"]; H.td  source  ];
      H.tr [ H.td [txt "Tags"]; H.td  tags ; ];
      H.tr [ H.td [txt "Pose"]; H.td pose ]
    ]    

  let link_button txt = B.button ~a_class:["is-link"] txt
  let danger_button txt = B.button ~a_class:["is-danger"] txt
  let success_button txt = B.button ~a_class:["is-success"] txt

  let cropped_image ~src ~alt =
    B.block [
      B.image ~a_class:["is-128x128"; "cropped-image"; "has-text-centered"]
        ~src ~alt ()
    ]

  let field_with_addon ~field ~addon =
    B.field ~a_class:["has-addons"] [
      field;
      addon
    ]

  let read_only_input value =
    B.input ~a:[H.a_input_type `Text; H.a_value value; H.a_readonly ()] ()

  let editable_input value =
    B.input ~a:[H.a_input_type `Text; H.a_value value] ()

  let tag data = H.span ~a:[a_class' "tag"] [data];

end
