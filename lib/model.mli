open Common

module Source : sig
  type t = [`External of Jstr.t | `Embedded of Jstr.t ] [@@deriving show, eq]
end

type pose = float

module Data : sig
  type t [@@deriving show,eq]

  val v: id:int -> source:Source.t -> name:Jstr.t -> tags:StringSet.t -> pose:pose -> t
  val image : t -> Jstr.t
  val name: t -> Jstr.t
  val tags: t -> StringSet.t
  val pose: t -> pose
  val source: t -> Source.t

  val set_name : Jstr.t -> t -> t
  val set_tags : StringSet.t -> t -> t
  val set_pose : pose -> t -> t

  val tags_is_subset : (t * 'a) Containers.List.t -> StringSet.t -> (t * 'a) Containers.List.t
  val id_equal : t -> t -> bool
end


module Scene: sig
  type t [@@deriving show, eq]
  val init : Data.t list -> t
  val selected : t -> Data.t option
  val update_selected : (Data.t -> Data.t option) -> t -> t

  val delete_selected: t -> t
  val data : t -> Data.t list
  val set_selected_ind : int -> t -> t
  val set_selected : Data.t -> t -> t
  val add_image : source:Source.t -> name:Jstr.t -> tags:StringSet.t -> pose:pose -> t -> t
end
