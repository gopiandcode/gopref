open Containers
open Common

type jstr = Jstr.t [@@deriving eq]
let pp_jstr fmt v =
  Format.pp_print_string fmt (Jstr.to_string v)
type string_set = StringSet.t  [@@deriving eq]
let[@warning "-32"] pp_string_set fmt v = StringSet.pp pp_jstr fmt v

type pose = float

module Source = struct
  type t = [`External of jstr | `Embedded of jstr ] [@@deriving show, eq]
end

module Data = struct

  type t = {
    id: int;
    source: Source.t;
    name: jstr;
    tags: string_set;
    pose: float
  } [@@deriving eq, show]

  let v ~id ~source ~name ~tags ~pose =
    {id;source;name;tags;pose}

  let image v = match v.source with `External s -> s | `Embedded d -> d
  let name v = v.name
  let tags v = v.tags
  let pose v = v.pose
  let source v= v.source

  let set_name name t = {t with name}
  let set_tags tags t = {t with tags}
  let set_pose pose t = {t with pose}

  let tags_is_subset ls filter =
    if StringSet.is_empty filter then ls
    else List.filter (fun (dat,_) -> StringSet.subset filter dat.tags) ls  

  let id_equal v v' = v.id = v'.id

end


module Scene = struct

  type t = {
    data: Data.t list;
    next_id: int;
    selected: (Data.t * int) option;
  } [@@deriving eq, show]

  let init data =
    let next_id = List.fold_left Fun.(flip @@ max % (fun v -> v.Data.id)) 0 data + 1 in
    {data; next_id; selected=None}


  let selected t = Option.map fst t.selected
  let update_selected f t =
    {t with
     selected=
       match t.selected with
       | None -> None
       | Some (sel, ind) -> Option.map (Fun.flip Pair.make ind) @@ f sel
    }

  let list_with ~value:vl ~at:ind data =
    List.to_iter data
      |> Iter.zip_i
      |> Iter.flat_map (fun (ind', vl') ->
        if ind' = ind then Iter.of_list [vl;vl'] else Iter.return vl'
      )
      |> Iter.to_list    

  let list_without ~ind data =
    let res = ref None in
    let result = 
      List.to_iter data
      |> Iter.zip_i
      |> Iter.flat_map (function
        | (ind', vl) when ind = ind' -> res := Some vl; Iter.empty
        | (_, vl) -> Iter.return vl
      ) |> Iter.to_list in
    result, !res

  let list_without_elt ~elt data =
    let res = ref None in
    let rec loop i acc = function
      | [] -> List.rev acc
      | h :: t when Data.id_equal h elt -> res := Some (h, i); List.rev_append acc t
      | h :: t ->
        loop (i+1) (h :: acc) t in
    let result = loop 0 [] data in
    result, !res

  let data = function
    | {selected=None; data;_} -> data
    | {selected=Some (value,ind); data; _} ->
      list_with ~value ~at:ind data

  let delete_selected t = {t with selected=None}

  let set_selected elt =
    function
    | {selected=None;data; _} as t ->
      let data, selected = list_without_elt ~elt data in
      selected
      |> Option.map (fun (selected,ind) ->
        {t with data; selected=Some (selected, ind)}
      )
      |> Option.get_or ~default:t
    | {selected=Some (value,ind'); data; _} as t ->
      let data = list_with ~value ~at:ind' data in
      let data, selected = list_without_elt ~elt data in
      selected
      |> Option.map (fun (selected,ind) ->
        {t with data; selected=Some (selected, ind)}
      )
      |> Option.get_or ~default:t


  let set_selected_ind ind =
    function
    | {selected=None;data; _} as t ->
      let data, selected = list_without ~ind data in
      selected
      |> Option.map (fun selected ->
        {t with data; selected=Some (selected, ind)}
      )
      |> Option.get_or ~default:t
    | {selected=Some (value,ind'); data; _} as t ->
      let data = list_with ~value ~at:ind' data in
      let data, selected = list_without ~ind data in
      selected
      |> Option.map (fun selected ->
        {t with data; selected=Some (selected, ind)}
      )
      |> Option.get_or ~default:t

  let add_image
        ~source ~name ~tags ~pose =
    function
    | {selected=None;next_id=id;_} as t ->
      {t with next_id = id + 1; selected=Some ({id; source;name;tags;pose}, 0)}
    | {selected=Some (value,ind); data; next_id=id} ->
      let data = list_with ~value ~at:ind data in
      {data; next_id=id + 1; selected=Some ({id; source;name;tags;pose}, 0)}

end
