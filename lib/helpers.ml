open Containers
open Brr

let file_to_data : File.t -> Jstr.t option Fut.t =
  fun file ->
  Jv.set Jv.global "file_data" (File.to_jv file);
  let blob = File.as_blob file in
  Blob.data_uri blob
  |> Fut.map (fun v -> v |> Result.map Option.some |> Console.log_if_error ~use:None)

module File_list : sig
  type t
  val of_input: Brr.El.t -> t
  val iter: t -> Brr.File.t Iter.t
  val extract_files: ?embed:bool -> t -> ([> `File ], Jstr.t) Containers.Pair.t Containers.Option.t Fut.t
end = struct

  type t = Jv.t

  let of_input input = Jv.get (El.to_jv input) "files"

  let iter items : _ Iter.t =
    let length = Jv.to_int @@ Jv.get items "length" in
    let get i = Brr.File.of_jv (Jv.call items "item" [| Jv.of_int i |]) in
    fun f -> for i = 0 to length - 1 do f (get i) done

  let extract_files ?(embed=false) data =
    let get_data item = `File, fun () -> file_to_data item in
    data
    |> iter
    |> Iter.map get_data
    |> Iter.fold (fun acc res ->
      match res with
      | `File, _ when embed -> Some res
      | _ when Option.is_none acc -> Some res
      | _ -> acc
    ) None
    |> Option.map (fun (ty, data) -> data () |> Fut.map (Option.map @@ Pair.make ty))
    |> Option.value ~default:(Fut.return None)

end

module Item_list = struct

  let iter items : _ Iter.t =
    let length = Ev.Data_transfer.Item_list.length items in
    let get i = Ev.Data_transfer.Item_list.item items i in
    fun f -> for i = 0 to length - 1 do f (get i) done

  let extract_files ?(embed=false) data =
    let get_data item =
      let kind = Ev.Data_transfer.Item.kind item in
      Console.(log [str "kind of item is "; kind]);
      match kind with
      | _ when Jstr.equal kind Ev.Data_transfer.Item.Kind.file ->
        Ev.Data_transfer.Item.get_file item
        |> Option.map (fun v -> `File, fun () -> file_to_data v)
      | _ when Jstr.equal kind Ev.Data_transfer.Item.Kind.string ->
        Some (`String, fun () -> Ev.Data_transfer.Item.get_jstr item |> Fut.map Option.some)
      | _ -> None in
    data
    |> iter
    |> Iter.filter_map get_data
    |> Iter.fold (fun acc res ->
      match res with
      | `File, _ when embed -> Some res
      | _ when Option.is_none acc -> Some res
      | _ -> acc
    ) None
    |> Option.map (fun (ty, data) -> data () |> Fut.map (Option.map @@ Pair.make ty))
    |> Option.value ~default:(Fut.return None)

end



module Data_transfer = struct

  let iter_items data : _ Iter.t =
    let items = Ev.Data_transfer.items data in
    Item_list.iter items

  let extract_data_string ?(embed=true) data =
    data
    |> Ev.Data_transfer.items
    |> Item_list.extract_files ~embed

end

