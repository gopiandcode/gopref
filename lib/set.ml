module type JV = sig
  type t
  val to_jv : t -> Jv.t
end

let cls = Jv.get Jv.global "Set"

module Make (Elt: JV) : sig
  type t
  val to_jv : t -> Jv.t
  val create : unit -> t
  val add : Elt.t -> t -> unit
  val mem : Elt.t -> t -> bool
  val rem : Elt.t -> t -> bool
  val iter : t -> (Elt.t -> unit) -> unit
  val size : t -> int
  val subset : t -> t -> bool
end = struct
  type t = Jv.t

  let to_jv v = v

  let create () = Jv.new' cls  [| |]

  let add (v: Elt.t) set =
    ignore @@ Jv.call set "add" [|Elt.to_jv v|]

  let mem (v: Elt.t) set =
    Jv.to_bool @@ Jv.call set "has" [|Elt.to_jv v|]

  let rem (v: Elt.t) set =
    Jv.to_bool @@ Jv.call set "delete" [|Elt.to_jv v|]

  let iter set (f: Elt.t -> unit) : unit =
    ignore @@ Jv.call set "forEach" [| Jv.repr f |]

  let size set = Jv.to_int @@ Jv.get set "size"

  let subset set_a set_b = Iter.for_all (fun v -> mem v set_b) (iter set_a)

end
